<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductModel extends Model
{
    protected $table = 'mst_product';

    public static function get($select = array(), $where = array())
    {
        $queries = ProductModel::query();
        //Nếu có chỉ định các column cần lấy
        $select ? $queries->select($select) : '';
        //Nếu có các điều kiện lọc
        $where ? $queries->where($where) : '';
        return $queries->first();       
    }

    public static function fetch($select = array(), $where = array(), $like = array(), $compare = array(), $orderBy = '', $sort = '', $perPage = null)
    {
        
        $queries = ProductModel::query();
        $select ? $queries->select($select) : '';
        $where ? $queries->where($where) : '';
        //Nếu có điều kiện lọc LIKE
        foreach($like as $column=>$value){
            $queries->where($column, 'like', '%'.$value.'%');
        }
        foreach($compare as $key=>$value){
            if($key == "priceFrom"){
                $queries->where("product_price", '>=', $value);
            }

            if($key == "priceTo"){
                $queries->where("product_price", '<=', $value);
            }
        }
        
        $orderBy != '' && $sort != '' ? $queries->orderBy($orderBy, $sort) : '';
        $results = $perPage ? $queries->paginate($perPage) : $queries->get();
        
        return $results;        
    }

    public static function add(array $arrAdd)
    {
        $product = new ProductModel;
        foreach($arrAdd as $field=>$value){
            $product->$field = $value;
        }
        return $product->save() ? true : false;
    }

    public static function updates($id, $arrUpdate)
    {
        return ProductModel::where('product_id', $id)->update($arrUpdate) ? true : false;
    }

    public static function removeRecord($id){
        return ProductModel::where('product_id', $id)->forceDelete() ? true : false;
    }
}
