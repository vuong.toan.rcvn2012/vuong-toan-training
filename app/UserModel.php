<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_users';
    
    public static function get($select = array(), $where = array())
    {
        $user = UserModel::query();
        //Nếu có chỉ định các column cần lấy
        $select ? $user->select($select) : '';
        //Nếu có các điều kiện lọc
        $where ? $user->where($where) : '';
        return $user->first();       
    }

    public static function fetch($select = array(), $where = array(), $like = array(), $orderBy = '', $sort = '', $perPage = null)
    {
        $user = UserModel::query();
        $select ? $user->select($select) : '';
        $where ? $user->where($where) : '';
        //Nếu có điều kiện lọc LIKE
        foreach($like as $column=>$value){
            $user->where($column, 'like', '%'.$value.'%');
        }
        $orderBy != '' && $sort != '' ? $user->orderBy($orderBy, $sort) : '';
        $listUser = $perPage ? $user->paginate($perPage) : $user->get();
        return $listUser;        
    }

    public static function add(array $userAdd)
    {
        $user = new UserModel;
        foreach($userAdd as $field=>$value){
            $user->$field = $value;
        }
        return $user->save() ? true : false;
    }

    public static function updates($id, $arrUpdate)
    {
        return UserModel::where('id', $id)->update($arrUpdate) ? true : false;
    }
}
