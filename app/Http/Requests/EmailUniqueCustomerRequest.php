<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailUniqueCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:mst_customer',
        ];
    }

    public function messages()
    {
        return [
            'email.required'    =>  'Email không được để trống.',
            'email.email'       =>  'Email chưa đúng định dạng.',
            'email.unique'      =>  'Email đã được đăng ký.',
        ];
    }
}
