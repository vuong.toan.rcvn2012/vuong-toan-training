<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|min:6',
            'email'         => 'required|email',
            'tel_num'       => 'required|regex:/^0[3,5,7,8,9][0-9]{8}$/',
            //'tel_num'       => 'required|regex:/^(03|05|07|08|09){8}$/',
            'address'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'customer_name.required' => 'Vui lòng nhập tên khách hàng.',
            'customer_name.min' => 'Tên khách hàng phải lớn hơn 5 ký tự.',
            'email.required'    =>  'Email không được để trống.',
            'email.email'       =>  'Email chưa đúng định dạng.',
            'tel_num.required'  =>  'Số điện thoại không được để trống.',
            'tel_num.regex'    =>   'Số điện thoại chưa đúng định dạng.',
            //'tel_num.digits'    =>  'Số điện thoại chưa đúng định dạng.',
            'address.required'  =>  'Địa chỉ không được để trống.',
        ];
    }
}
