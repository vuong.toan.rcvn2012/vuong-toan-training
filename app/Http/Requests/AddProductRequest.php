<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name'      => 'required|min:6',
            'product_price'     => 'required|integer|min:1',
            'is_sales'          => 'required',
            'product_image'     => 'mimes:jpg,png,jpeg|max:2048|dimensions:max_width=1025,max_height=1025'
        ];
    }

    public function messages()
    {
        return [
            'product_name.required'     =>  'Vui lòng nhập tên sản phẩm.',
            'product_name.min'          =>  'Tên phải lớn hơn 5 ký tự',
            'product_price.required'    =>  'Giá bán không được để trống',
            'product_price.integer'     =>  'Giá bán chỉ được nhập số',
            'product_price.min'         =>  'Giá bán không được nhỏ hơn 0',
            'is_sales.required'         =>  'Trạng thái không được để trống',
            'product_image.max'         =>  'Dung lượng hình ảnh không quá 2Mb',
            'product_image.mimes'       =>  'Chỉ cho phép upload các file hình ảnh (jpg,png,jpeg).',
            'product_image.dimensions'  =>  'Kích thước không quá 1024px',
        ];
    }
}
