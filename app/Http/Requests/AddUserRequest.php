<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:6',
            'email' => 'required|email|unique:mst_users',
            'password'  => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            'repassword'=> 'required|same:password',
            'group'     => 'required',
        ];
    }

    public function messages(){
        return [
            'name.required'     =>  'Vui lòng nhập tên người sử dụng.',
            'name.min'          =>  'Tên phải lớn hơn 5 ký tự.',
            'email.required'    =>  'Email không được để trống.',
            'email.email'       =>  'Email không đúng định dạng.',
            'email.unique'      =>  'Email đã được đăng ký.',
            'password.required' =>  'Mật khẩu không được để trống.',
            'password.min'      =>  'Mật khẩu phải hơn 5 ký tự.',
            'password.regex'    =>  'Mật khẩu không bảo mật (Phải có chữ HOA, thường & số)',
            'repassword.required'   =>  'Xác nhận mật khẩu không được để trống.',
            'repassword.same'   =>  'Mật khẩu và xác nhận mật khẩu không chính xác.',
            'group.required'    =>  'Vui lòng chọn 1 Nhóm chỉ định.'
        ];
    }
}
