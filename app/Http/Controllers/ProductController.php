<?php
namespace App\Http\Controllers;

use Auth;
use App\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AddProductRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ProductController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function listProduct(Request $request)
    {
        $data = array();
        $data['currentView'] = 'listProduct';

        $queryUrl = '';
        $select = array('product_id', 'product_name', 'description', 'product_price', 'is_sales', 'created_at', 'product_image');
        $where = array();
        $like = array();
        $compare = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;

        //Nếu phân trang kèm theo điều kiện search
        if ($request->nameLike != ''){
            $like['product_name'] = $request->nameLike;
            $queryUrl .= 'nameLike='.$request->nameLike.'&';
        }
        if ($request->isSales != ''){
            $where['is_sales'] = (int)$request->isSales;
            $queryUrl .= 'isSales='.$request->isSales.'&';
        }
        if ($request->priceFrom != ''){
            $compare['priceFrom'] = $request->priceFrom;
            $queryUrl .= 'priceFrom='.$request->priceFrom.'&';
        }
        if ($request->priceTo != ''){
            $compare['priceTo'] = $request->priceTo;
            $queryUrl .= 'priceTo='.$request->priceTo.'&';
        }

        /**
         * Hàm trả về danh sách các Khách hàng thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $customers
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        $products = ProductModel::fetch($select, $where, $like, $compare, $orderBy, $sort, $perPage);
        
        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $products->withPath('list-product?'.rtrim($queryUrl,'&'));
        } else {
            $products->withPath('list-product');
        }
        
        $data['products'] = $products;
        
        if($request->has("paginate")){
            return view('product/dataTableProducts')->with('arrData', $data);
        }else{
            return view('product/listProduct')->with('arrData', $data);
        }
        
    }

    public function search(Request $request)
    {
        //var_dump("search");die();
        $data = array();
        $queryUrl = '';
        $select = array('product_id', 'product_name', 'description', 'product_price', 'is_sales', 'created_at');
        $where = array();
        $like = array();
        $compare = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;
        
        if ($request->nameLike != ''){
            $like['product_name'] = $request->nameLike;
            $queryUrl .= 'nameLike='.$request->nameLike.'&';
        }
        if ($request->isSales != ''){
            $where['is_sales'] = (int)$request->isSales;
            $queryUrl .= 'isSales='.$request->isSales.'&';
        }
        if ($request->priceFrom != ''){
            $compare['priceFrom'] = $request->priceFrom;
            $queryUrl .= 'priceFrom='.$request->priceFrom.'&';
        }
        if ($request->priceTo != ''){
            $compare['priceTo'] = $request->priceTo;
            $queryUrl .= 'priceTo='.$request->priceTo.'&';
        }
        

        //var_dump($compare);die();
        
        /**
         * Hàm trả về danh sách các khách hàng thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $users
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        
        $products = ProductModel::fetch($select, $where, $like, $compare, $orderBy, $sort, $perPage);
        //var_dump($products);die();
        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $products->withPath('list-product?'.rtrim($queryUrl,'&'));
        } else {
            $products->withPath('list-product');
        }

        $data['products'] = $products;
        
        return view('product/dataTableProducts')->with('arrData', $data);
    }

    public function hoverShowImage(Request $request)
    {
        $result = array();
        if($request->product_id){
            $select = array('product_image');
            $where = array('product_id' => $request->product_id);
            $product = ProductModel::get($select, $where);
            //echo $product->product_image;
            if($product->product_image != ''){
                $result['errorCode'] = 0;
                $result['status'] = 'success';
                $result['message'] = $product->product_image;
            }else{
                $result['errorCode'] = 2;
                $result['status'] = 'error';
                $result['message'] = 'Sản phẩm không có hình ảnh';
            }
            
        }else{
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Lỗi khi tìm ảnh của sản phẩm.';
        }
        echo json_encode($result);
    }

    public function deleteProduct(Request $request)
    {
        $result = array();
        /**
         * Hàm trả về true hoặc false sau khi xóa record khỏi db
         *
         * @param array $id id của product cần xóa
         * @return bool true hoặc false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */

        if(ProductModel::removeRecord($request->product_id)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Thành công';
        }else{
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Có lỗi xảy ra trong hệ thống khi xóa, vui lòng thử lại sau.';
        }
        echo json_encode($result);
    }

    public function addProduct(){
        $data = array();
        $data['currentView'] = 'listProduct';

        return view('product/addProduct')->with('arrData', $data);
    }

    public function submitAddProduct(AddProductRequest $request)
    {
        //var_dump($request->product_image_tmp);die();
        //Nếu có up hình
        
        if($request->hasFile('product_image')){
            //$image = $request->file('inputUploadImage');
            $file = $request->product_image;
            // var_dump($file);die();
            //Lấy Tên files
            $name = $file->getClientOriginalName();

            //Lấy Đuôi File
            $extension = $file->getClientOriginalExtension();

            //Lấy đường dẫn tạm thời của file
            $tmpPath = $file->getRealPath();

            //Lấy kích cỡ của file đơn vị tính theo bytes
            $size = $file->getSize();

            //Lấy kiểu file
            $type = $file->getMimeType();

            $dir = 'imagesUpload';
            $filename = uniqid().'_'.time().'_'.date('Ymd').'_'.$name;
            $file->move($dir, $filename);
            $pathImage = $dir."/".$filename;
        //Nếu load hình từ tmp đã check validation trước đó 
        }else if($request->product_image_tmp != ''){
            $dir = 'imagesProductUpload';
            $image_64 = $request->product_image_tmp;
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

            $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

            // find substring fro replace here eg: data:image/png;base64,

            $image = str_replace($replace, '', $image_64); 

            $image = str_replace(' ', '+', $image); 

            $imageName = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
            //var_dump($imageName);die();
            Storage::disk('public')->put($dir.'/'.$imageName, base64_decode($image));
            $pathImage = "storage/".$dir.'/'.$imageName;
        }else{
            $pathImage = "";
        }

        $validated = $request->validated();
   
        $productId = $this->createProductIdAuto($validated['product_name']);

        $arrAdd = array(
            'product_id'        =>  $productId,
            'product_name'      =>  $validated['product_name'],
            'product_image'     =>  $pathImage,
            'product_price'     =>  $validated['product_price'],
            'is_sales'          =>  $validated['is_sales'],
            'description'       =>  $request->description,
        );
        if(ProductModel::add($arrAdd)){
            return redirect()->route('getListProduct');
        }else{
            return redirect()->back()->with('status', 'Có lỗi xảy ra trong quá trình thêm vào cơ sở dữ liệu, thử lại sau!');
        }
        
    }

    public function editProduct(Request $request)
    {
        $data = array();
        $data['currentView'] = 'listProduct';
        $product = ProductModel::get(array("*"), array("product_id" =>  $request->product_id));
        
        $data['product'] = $product;

        return view('product/editProduct')->with('arrData', $data);
    }

    public function submitEditProduct(AddProductRequest $request)
    {
        $validated = $request->validated();

        $productId = $request->product_id;
        //Kiểm tra thay đổi tên nhưng ko được thay chữ cái đầu
        $product = ProductModel::get(array('product_name','product_image'), array('product_id'  =>  $productId));
        if(substr($product->product_name, 0, 1) != substr($validated['product_name'], 0, 1)){
            //Nếu tên sản phẩm có chữ cái đầu khác trước thì báo lỗi vì sẽ ảnh hưởng đến productId
            return redirect()->back()->with('status', 'Tên sản phẩm phải có chữ cái đầu giống hiện tại!')->withInput();
        }
    
        //xử lý ảnh
        if($request->hasFile('product_image')){
            //$image = $request->file('inputUploadImage');
            $file = $request->product_image;
            
            //Lấy Tên files
            $name = $file->getClientOriginalName();

            //Lấy Đuôi File
            $extension = $file->getClientOriginalExtension();

            //Lấy đường dẫn tạm thời của file
            $tmpPath = $file->getRealPath();

            //Lấy kích cỡ của file đơn vị tính theo bytes
            $size = $file->getSize();

            //Lấy kiểu file
            $type = $file->getMimeType();

            $dir = 'imagesUpload';
            $filename = uniqid().'_'.time().'_'.date('Ymd').'_'.$name;
            $file->move($dir, $filename);
            $pathImage = $dir."/".$filename;
        }else if($request->product_image_tmp != ''){
            $dir = 'imagesProductUpload';
            $image_64 = $request->product_image_tmp;
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

            $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

            // find substring fro replace here eg: data:image/png;base64,

            $image = str_replace($replace, '', $image_64); 

            $image = str_replace(' ', '+', $image); 

            $imageName = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
            //var_dump($imageName);die();
            Storage::disk('public')->put($dir.'/'.$imageName, base64_decode($image));
            $pathImage = "storage/".$dir.'/'.$imageName;
        }else{
            //var_dump($request->flag_delete_image);die();
            $pathImage = $request->flag_delete_image == 1? "" : $product->product_image;
        }

        $arrUpdate = array(
            'product_name'      =>  $validated['product_name'],
            'product_image'     =>  $pathImage,
            'product_price'     =>  $validated['product_price'],
            'is_sales'          =>  $validated['is_sales'],
            'description'       =>  $request->description,
        );
        if(ProductModel::updates($productId, $arrUpdate)){
            return redirect()->route('getListProduct');
        }else{
            return redirect()->back()->with('status', 'Có lỗi xảy ra trong quá trình cập nhật vào cơ sở dữ liệu, thử lại sau!');
        }
    }

    private function createProductIdAuto(String $productName)
    {
        $firstChar = substr($productName, 0, 1);
        $result = $firstChar."000000001";
        for($i=1;$i<=999999999;$i++){
            $strNum = "";
            for($j=strlen($i);$j<9;$j++){
                $strNum .= "0";
            }
            $productIdTmp = $firstChar.$strNum.$i;
            $getProduct = ProductModel::get(array('product_name'), array('product_id'   =>  $productIdTmp));
            if($getProduct == NULL){
                $productIdFinal = $productIdTmp;
                break;
            }
        }
        return $productIdFinal;

    }
}
