<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\AddProductRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class OrderController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function listOrder()
    {
        $data = array();
        $data['currentView'] = 'listOrder';
        
        return view('orders/listOrder')->with('arrData', $data);
    }

    public function detailOrder(Request $request){
        $data = array();
        $data['currentView'] = 'listOrder';
        $data['orderId'] = $request->orderId;
        
        return view('orders/detailOrder')->with('arrData', $data);
    }

}
