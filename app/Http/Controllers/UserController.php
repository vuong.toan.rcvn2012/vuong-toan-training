<?php
namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class UserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function login()
    {
        //Nếu User đã đăng nhập
        if (Auth::check()) { 
            //Không cần check active vì sau khi qua trang listuser middleware sẽ check
            return redirect()->route('getListProduct');
        //Nếu User mới chưa đăng nhập -> Hiển thị màn hình Login    
        } else {
            return view('user/login');
        }
    }

    public function submitLogin(LoginUserRequest $request)
    {
        $validated = $request->validated();
    
        $login = [
            'email' => $validated['email'],
            'password' => $validated['password'],
        ];
        $remember = $request->remember == "true" ? true : false;
        //Nếu tài khoản và mật khẩu hợp lệ
        if (Auth::attempt($login, $remember)) {
            /**
             * Hàm update địa chỉ IP với lần truy cập cuối vào database
             *
             * @param int $id ID của user cần update
             * @param array $arrUpdate mảng danh sách các column & giá trị update
             * @return bool
             */

            $arrUpdate = array(
                'last_login_ip' =>  $request->getClientIp(),
                'last_login_at' =>  Carbon::now()->toDateTimeString(),
            );
            UserModel::updates((int)Auth::id(), $arrUpdate);
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Đăng nhập thành công.';
            //return redirect()->route('getListUser');
        //Tài khoản hoặc mật khẩu không đúng
        } else {
            // return redirect()->back()->with('status', 'Email hoặc Password không chính xác');
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Email hoặc Password không chính xác.';
        }
        echo json_encode($result);
    }

    public function logOut()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }

    public function listUser(Request $request)
    {     
        $data = array();
        $data['currentView'] = 'listUser';
        $data['metaTitle'] = 'DANH SÁCH USER';

        $queryUrl = '';
        $select = array('id', 'name', 'email', 'group_role', 'is_active', 'created_at');
        $where = array('is_delete' => 0);
        $like = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;

        //Nếu phân trang kèm theo điều kiện search
        if ($request->nameLike != ''){
            $like['name'] = $request->nameLike;
            $queryUrl .= 'nameLike='.$request->nameLike.'&';
        }
        if ($request->emailLike != ''){
            $like['email'] = $request->emailLike;
            $queryUrl .= 'emailLike='.$request->emailLike.'&';
        }
        if ($request->groupRole != ''){
            $where['group_role'] = $request->groupRole;
            $queryUrl .= 'groupRole='.$request->groupRole.'&'; 
        }
        if ($request->isActive != ''){
            $where['is_active'] = (int)$request->isActive;
            $queryUrl .= 'isActive='.$request->isActive.'&';
        }  

        /**
         * Hàm trả về danh sách các User thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $users
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        $users = UserModel::fetch($select, $where, $like, $orderBy, $sort, $perPage);

        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $users->withPath('list-user?'.rtrim($queryUrl,'&'));
        } else {
            $users->withPath('list-user');
        }
        
        $data['users'] = $users;

        if($request->has("paginate")){
            //var_dump($request->paginate);die();
            return view('user/dataTableUsers')->with('arrData', $data);
        }else{
            return view('user/listUser')->with('arrData', $data);
        }

        
        
    }

    public function searchUser(Request $request)
    {      
        $data = array();
        $queryUrl = '';
        $select = array('id', 'name', 'email', 'group_role', 'is_active', 'created_at');
        $where = array('is_delete' => 0);
        $like = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;

        if ($request->nameSearch != ''){
            $like['name'] = $request->nameSearch;
            $queryUrl .= 'nameLike='.$request->nameSearch.'&';
        }
        if ($request->emailSearch != ''){
            $like['email'] = $request->emailSearch;
            $queryUrl .= 'emailLike='.$request->emailSearch.'&';
        }
        if ($request->groupSearch != ''){
            $where['group_role'] = $request->groupSearch;
            $queryUrl .= 'groupRole='.$request->groupSearch.'&'; 
        }
        if ($request->statusSearch != ''){
            $where['is_active'] = (int)$request->statusSearch;
            $queryUrl .= 'isActive='.$request->statusSearch.'&';
        }

        /**
         * Hàm trả về danh sách các User thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $users
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        $users = UserModel::fetch($select, $where, $like, $orderBy, $sort, $perPage);

        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $users->withPath('list-user?'.rtrim($queryUrl,'&'));
        } else {
            $users->withPath('list-user');
        }

        $data['users'] = $users;
        return view('user/dataTableUsers')->with('arrData', $data);
    }

    public function popupAddUser()
    {
        return view('user/popupAddUser');
    }

    public function submitAddUser(AddUserRequest $request)
    {
        $result = array();
        $validated = $request->validated();
        $userAdd = $validated;

        $userAdd = array(
            'name'      =>  $validated['name'],
            'email'     =>  $validated['email'],
            'password'  =>  Hash::make($validated['password']),
            'group_role'=>  $validated['group'],
            'is_active' =>  $request->active,
            'is_delete' =>  0,
        );
        /**
         * Hàm thêm 1 User mới vào database
         *
         * @param array $userAdd mảng danh sách các column & value của User mới
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (UserModel::add($userAdd)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Thêm User thành công!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Lỗi xảy ra trong quá trình thêm vào cơ sở dữ liệu.';
        }

        echo json_encode($result);
    }

    public function popupEditUser(Request $request)
    {        
        /**
         * Hàm trả về thông tin user đang Edit
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng danh sách các điều kiện lọc
         * @return object $user
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        $select = array('id', 'name', 'email', 'group_role', 'is_active');
        $where = array('id' => $request->idUser);
        $user = UserModel::get($select, $where);
        $userEdit = $user->toArray();
        return view('user/popupEditUser')->with('userEdit', $userEdit);
    }

    public function submitEditUser(EditUserRequest $request)
    {
        //var_dump("validated");die();
        $validated = $request->validated();
        $userId = (int)$request->id;
        $user = UserModel::get(array('id', 'password'), array('id' => $userId));

        $password = $validated['password'] == "" ? $user->password : Hash::make($validated['password']);

        $arrUpdate = array(
            'name'      =>  $validated['name'],
            //'email'     =>  $request->email,
            'password'  =>  $password,
            'group_role'=>  $validated['group'],
            'is_active' =>  $request->active,
            //'is_delete' =>  0
        );
        
        /**
         * Hàm update thông tin User
         *
         * @param int $userId Id của User cần update
         * @param array $arrUpdate mảng danh sách các column & value của User update
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (UserModel::updates($userId, $arrUpdate)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Cập nhật User thành công!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Lỗi xảy ra trong quá trình cập nhật thông tin User vào cơ sở dữ liệu.';
        }

        echo json_encode($result);
    }

    public function deleteUser(Request $request)
    {
        //Hàm này SẼ KHÔNG XÓA mà chỉ cập nhật column is_delete = 1
        $result = array();
        $userId = (int)$request->id;
        $arrUpdate = array('is_delete'=>1);

        /**
         * Hàm update giá trị is_delete = 1
         *
         * @param int $userId Id của User cần update
         * @param array $arrUpdate mảng danh sách các column & value của User update
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (UserModel::updates($userId, $arrUpdate)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'User '.$request->name.' đã được xóa khỏi danh sách!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Có lỗi xảy ra trong quá trình xóa User '.$request->name.', vui lòng thử lại sau!';
        }
 
        echo json_encode($result);die();
    }

    public function changeStatusUser(Request $request)
    {
        $result = array();
        $userId = (int)$request->id;
        //Giá trị update sẽ là giá trị ngược lại hiện tại
        $arrUpdate = array('is_active'=> $request->isActiveCurrent == 1 ? 0 : 1);

        /**
         * Hàm update giá trị is_active
         *
         * @param int $userId Id của User cần update
         * @param array $arrUpdate mảng danh sách các column & value của User update
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (UserModel::updates($userId, $arrUpdate)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'User '.$request->name.' đã chuyển trạng thái thành công!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Có lỗi xảy ra trong quá trình đổi trạng thái User '.$request->name.', vui lòng thử lại sau!';
        }
 
        echo json_encode($result);die();
    }
}
