<?php
namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\CustomerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CustomerExport;
use App\Imports\CustomersImport;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AddCustomerRequest;
use App\Http\Requests\EditCustomerRequest;
use App\Http\Requests\EmailUniqueCustomerRequest;

class CustomerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function listCustomer(Request $request)
    {
        $data = array();
        $data['currentView'] = 'listCustomer';
        $data['metaTitle'] = 'DANH SÁCH KHÁCH HÀNG';

        $queryUrl = '';
        $select = array('customer_id', 'customer_name', 'email', 'address', 'tel_num', 'created_at');
        $where = array();
        $like = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;

        //Nếu phân trang kèm theo điều kiện search
        if ($request->nameLike != ''){
            $like['customer_name'] = $request->nameLike;
            $queryUrl .= 'nameLike='.$request->nameLike.'&';
        }
        if ($request->emailLike != ''){
            $like['email'] = $request->emailLike;
            $queryUrl .= 'emailLike='.$request->emailLike.'&';
        }
        if ($request->addressLike != ''){
            $like['address'] = $request->addressLike;
            $queryUrl .= 'addressLike='.$request->addressLike.'&';
        }
        if ($request->isActive != ''){
            $where['is_active'] = (int)$request->isActive;
            $queryUrl .= 'isActive='.$request->isActive.'&';
        }

        /**
         * Hàm trả về danh sách các Khách hàng thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $customers
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        $customers = CustomerModel::fetch($select, $where, $like, $orderBy, $sort, $perPage);
        
        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $customers->withPath('list-customer?'.rtrim($queryUrl,'&'));
        } else {
            $customers->withPath('list-customer');
        }
        
        $data['customers'] = $customers;

        if($request->has("paginate")){
            return view('customer/dataTableCustomers')->with('arrData', $data);
        }else{
            return view('customer/listCustomer')->with('arrData', $data);
        }
    }

    public function searchCustomer(Request $request)
    {
        $data = array();
        $queryUrl = '';
        $select = array('customer_id', 'customer_name', 'email', 'address', 'tel_num', 'created_at');
        $where = array();
        $like = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;
        
        if ($request->nameLike != ''){
            $like['customer_name'] = $request->nameLike;
            $queryUrl .= 'nameLike='.$request->nameLike.'&';
        }
        if ($request->emailLike != ''){
            $like['email'] = $request->emailLike;
            $queryUrl .= 'emailLike='.$request->emailLike.'&';
        }
        if ($request->addressLike != ''){
            $like['address'] = $request->addressLike;
            $queryUrl .= 'addressLike='.$request->addressLike.'&';
        }
        if ($request->isActive != ''){
            $where['is_active'] = (int)$request->isActive;
            $queryUrl .= 'isActive='.$request->isActive.'&';
        }
        
        /**
         * Hàm trả về danh sách các khách hàng thỏa điều kiện
         *
         * @param array $select mảng danh sách các column cần lấy
         * @param array $where mảng các điều kiện lọc
         * @param array $likes mảng các trường so sánh Like (gần đúng) nếu có
         * @param string $orderBy tên column cần sắp xếp 
         * @param string $sort chiều sắp xếp desc or asc
         * @param int $perPage Số lượng record mỗi page (nếu muốn dùng phân trang) default = null
         * @return $users
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        
        $customers = CustomerModel::fetch($select, $where, $like, $orderBy, $sort, $perPage);

        $queryUrl = rtrim($queryUrl,'&');
        if ($queryUrl != ''){
            $customers->withPath('list-customer?'.rtrim($queryUrl,'&'));
        } else {
            $customers->withPath('list-customer');
        }

        $data['customers'] = $customers;
        
        return view('customer/dataTableCustomers')->with('arrData', $data);
    }

    public function submitEditCustomer(EditCustomerRequest $request)
    {
        $validated = $request->validated();
        $customerId = (int)$request->customer_id;  
        //Lấy thông tin email hiện tại của khách hàng để so sánh nếu có thay đổi thì check unique
        $customerEditing = CustomerModel::get(array('email'), array('customer_id' => $customerId));
        if($customerEditing->email != $validated['email']){
            $validator = Validator::make($request->all(), [
                'email' => 'unique:mst_customer',
                ],
                [
                    'email.unique'      =>  'Email đã được đăng ký, vui lòng thử email khác.',
                ]
            );
            //Nếu email có thay đổi và trùng với 1 email khác
            if ($validator->fails()) {
                $result['errorCode'] = 2;
                $result['status'] = 'error';
                $result['message'] = $validator->errors()->all();
                echo json_encode($result);
                die();
            }
        }

        $arrUpdate = array(
            'customer_name' =>  $validated['customer_name'],
            'email'         =>  $validated['email'],
            'address'       =>  $validated['address'],
            'tel_num'         =>  $validated['tel_num'],
        );
        /**
         * Hàm update thông tin Khách hàng
         *
         * @param int $customerId Id của khách hàng cần update
         * @param array $arrUpdate mảng danh sách các column & value của Khách hàng update
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (CustomerModel::updates($customerId, $arrUpdate)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Cập nhật khách hàng thành công!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Lỗi xảy ra trong quá trình cập nhật thông tin khách hàng vào cơ sở dữ liệu.';
        }
        echo json_encode($result);
    }

    public function exportCsv(Request $request)
    {
        $select = array('customer_name', 'email', 'tel_num', 'address');
        $where = array();
        $like = array();
        $orderBy = 'created_at';
        $sort = 'desc';
        $perPage = 10;

        //Nếu có điều kiện lọc
        if ($request->nameLike != ''){
            $like['customer_name'] = $request->nameLike;
        }
        if ($request->emailLike != ''){
            $like['email'] = $request->emailLike;
        }
        if ($request->addressLike != ''){
            $like['address'] = $request->addressLike;
        }
        if ($request->isActive != ''){
            $where['is_active'] = (int)$request->isActive;
        }

        $customers = CustomerModel::fetch($select, $where, $like, $orderBy, $sort, $perPage);
        $export = new CustomerExport($customers->toArray()['data']);
                
        return Excel::download($export, 'customers.csv');
    }

    public function importCsv(Request $request)
    {
        if($request->hasFile('attachment')){
            $file = $request->attachment;

            //Lấy Tên files
            $name = $file->getClientOriginalName();

            //Lấy Đuôi File
            $extension = $file->getClientOriginalExtension();

            //Lấy đường dẫn tạm thời của file
            $tmpPath = $file->getRealPath();

            //Lấy kích cỡ của file đơn vị tính theo bytes
            $size = $file->getSize();

            //Lấy kiểu file
            $type = $file->getMimeType();

            
            if($extension != "csv"){
                echo 'File up lên không đúng định dạng CSV';die();
            }
            
            $dataCsv = array_map('str_getcsv', file($tmpPath));
            //Xóa dòng Heading
            unset($dataCsv[0]);
            foreach($dataCsv as $key=>$item){
                if(count($item) == 4){
                    $requestValidateRow = new Request();
                    $requestValidateRow['customer_name'] = $item[0];
                    $requestValidateRow['email'] = $item[1];
                    $requestValidateRow['tel_num'] = $item[2];
                    $requestValidateRow['address'] = $item[3];
                    //var_dump($requestValidateRow->customer_name);die();

                    $validator = Validator::make($requestValidateRow->all(), [
                        'customer_name' => 'required|min:6',
                        'email'         => 'required|email|unique:mst_customer',
                        'tel_num'       => 'required|regex:/^0[3,5,7,8,9][0-9]{8}$/',
                        'address'       => 'required',
                    ],[
                        'customer_name.required' => 'Vui lòng nhập tên khách hàng',
                        'customer_name.min' =>  'Tên khách hàng phải lớn hơn 5 ký tự',
                        'email.required'    =>  'Email không được để trống',
                        'email.email'       =>  'Email chưa đúng định dạng',
                        'email.unique'      =>  'Email đã được đăng ký',
                        'tel_num.required'  =>  'Số điện thoại không được để trống',
                        'tel_num.regex'    =>   'Số điện thoại chưa đúng định dạng',
                        'address.required'  =>  'Địa chỉ không được để trống',
                    ]);

                    if ($validator->fails()) {
                        $errs = "Lỗi: ";
                        foreach($validator->errors()->all() as $err){
                            $errs .= $err.", ";
                        }
                        
                        $item[4] = '<span class="text-danger">'.trim(trim($errs),",").'</span>';
                    }else{
                        //Nếu pass validate => import vào db
                        //$item[4] = '<span class="text-success">Imported</span>';

                        $customerAdd = array(
                            'customer_name'     =>  $requestValidateRow['customer_name'],
                            'email'             =>  $requestValidateRow['email'],
                            'tel_num'           =>  $requestValidateRow['tel_num'],
                            'address'           =>  $requestValidateRow['address']
                        );
                        if (CustomerModel::add($customerAdd)){
                            $item[4] = '<span class="text-success">Imported</span>';
                        }else{
                            $item[4] = '<span class="text-danger">Lỗi trong quá trình thêm vào database.</span>';
                        }
                        
                    }                    
                    $dataCsv[$key] = $item;
                     
                }else{
                    //xóa mấy data dư thừa
                    unset($dataCsv[$key]);
                }
            }
            
            //lập lại chỉ mục
            $dataCsv = array_values($dataCsv);
                        
            return view('customer/dataTableCustomersImport', compact('dataCsv'));
            //Excel::import(new CustomersImport, '../public/csvImportCustomerUpload/'.$filename);
            //echo 0;
        }
    }

    public function popupAddCustomer()
    {
        return view('customer/popupAddCustomer');
    }

    public function submitAddCustomer(AddCustomerRequest $request)
    {
        $result = array();
        //Data validated
        $validated = $request->validated();
        $customerAdd = $validated;
        
        /**
         * Hàm thêm 1 Khách hàng mới mới vào database
         *
         * @param array $userAdd mảng danh sách các column & value của khách hàng mới
         * @return bool true:false
         * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
         */
        if (CustomerModel::add($customerAdd)){
            $result['errorCode'] = 0;
            $result['status'] = 'success';
            $result['message'] = 'Thêm khách hàng thành công!';
        } else {
            $result['errorCode'] = 1;
            $result['status'] = 'error';
            $result['message'] = 'Lỗi xảy ra trong quá trình thêm khách hàng vào cơ sở dữ liệu.';
        }
        echo json_encode($result);
    }
}
