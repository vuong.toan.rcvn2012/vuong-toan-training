<?php
namespace App\Http\Middleware;

use Auth;
use Closure;
use App\UserModel;
use Illuminate\Http\Request;

class checkAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Nếu đã đăng nhập
        if (Auth::check()){
            $select = array('id', 'is_active', 'name');
            $where = array('id' => Auth::id());

            /**
             * Hàm trả về 1 User
             *
             * @param array $select mảng danh sách các column cần lấy
             * @param array $where mảng danh sách các điều kiện lọc
             * @return object $user
             * @author Vương Toàn RiverCrane <vuong.toan.rcvn2012@gmail.com>
             */
            $user = UserModel::get($select, $where);

            //Nếu user đang hoạt động -> redirect sang trang yêu cầu
            if ($user->is_active == 1){
                return $next($request);
            //Nếu user đang tạm khóa -> đăng xuất & redirect trang Login kèm thông báo lỗi 
            } else {
                Auth::logout();
                return redirect('login')->with('status', 'Tài khoản của '.$user->name.' đang bị khóa.');
            }
        //Nếu chưa đăng nhập -> redirect trang Login
        } else {
            return redirect()->route('getLogin');
        }
        
    }
}
