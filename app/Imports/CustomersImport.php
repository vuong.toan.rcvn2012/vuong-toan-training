<?php

namespace App\Imports;

use App\CustomerModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomersImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new CustomerModel([
            'customer_name' =>  $row['ten_khach_hang'],
            'email'         =>  $row['email'],
            'tel_num'       =>  $row['telnum'],
            'address'       =>  $row['dia_chi'],
        ]);
    }
}
