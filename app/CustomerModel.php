<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CustomerModel extends Model
{
    protected $table = 'mst_customer';
    protected $primaryKey = 'customer_id';
    protected $fillable = ['customer_name','email','tel_num','address'];

    public static function get($select = array(), $where = array())
    {
        $queries = CustomerModel::query();
        //Nếu có chỉ định các column cần lấy
        $select ? $queries->select($select) : '';
        //Nếu có các điều kiện lọc
        $where ? $queries->where($where) : '';
        return $queries->first();       
    }

    public static function fetch($select = array(), $where = array(), $like = array(), $orderBy = '', $sort = '', $perPage = null)
    {
        
        $queries = CustomerModel::query();
        $select ? $queries->select($select) : '';
        $where ? $queries->where($where) : '';
        //Nếu có điều kiện lọc LIKE
        foreach($like as $column=>$value){
            $queries->where($column, 'like', '%'.$value.'%');
        }
        $orderBy != '' && $sort != '' ? $queries->orderBy($orderBy, $sort) : '';
        $results = $perPage ? $queries->paginate($perPage) : $queries->get();
        
        return $results;        
    }

    public static function add(array $userAdd)
    {
        $user = new CustomerModel;
        foreach($userAdd as $field=>$value){
            $user->$field = $value;
        }
        return $user->save() ? true : false;
    }

    public static function updates($id, $arrUpdate)
    {
        return CustomerModel::where('customer_id', $id)->update($arrUpdate) ? true : false;
    }
}
