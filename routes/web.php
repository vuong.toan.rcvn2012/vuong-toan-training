<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', ['as' => 'getLogin', 'uses' => 'UserController@login']);
Route::post('/login', ['as' => 'postLogin', 'uses' => 'UserController@submitLogin']);
Route::get('/logout', ['as' => 'getLogout', 'uses' => 'UserController@logOut']);

//các trang được xem khi đang trong trạng thái đăng nhập
Route::group(['middleware' => 'checkAdminLogin'], function() {
    Route::get('/', function(){
        return redirect('list-user');
    });
    //Route user
	Route::get('/list-user', ['as' => 'getListUser', 'uses' => 'UserController@listUser']);
    //Xóa user ra khỏi danh sách => is_delete = 1
    Route::post('/delete-user', ['as' => 'deleteUser', 'uses' => 'UserController@deleteUser']);
    //Change status user
    Route::post('/change-status-user', ['as' => 'postBlockUser', 'uses' => 'UserController@changeStatusUser']);
    //Tìm kiếm User Ajax
    Route::post('/search-user', ['as' => 'postSearchUser', 'uses' => 'UserController@searchUser']);
    //Ajax popup add user
    Route::post('/popup-add-user', ['as' => 'postPopupAddUser', 'uses' => 'UserController@popupAddUser']);
    //Ajax submit add user
    Route::post('/submit-add-user', ['as' => 'postSubmitAddUser', 'uses' => 'UserController@submitAddUser']);
    //Ajax popup edit user
    Route::post('/popup-edit-user', ['as' => 'postPopupEditUser', 'uses' => 'UserController@popupEditUser']);
    //Ajax submit edit user
    Route::post('/submit-edit-user', ['as' => 'postSubmitEditUser', 'uses' => 'UserController@submitEditUser']);

    //Route khách hàng
	Route::get('/list-customer', ['as' => 'getListCustomer', 'uses' => 'CustomerController@listCustomer']);
    //Tìm kiếm khách hàng Ajax
    Route::post('/search-customer', ['as' => 'postSearchCustomer', 'uses' => 'CustomerController@searchCustomer']);
    //Ajax submit edit user
    Route::post('/submit-edit-customer', ['as' => 'postSubmitEditCustomer', 'uses' => 'CustomerController@submitEditCustomer']);
    //Export CSV
    Route::get('/export-csv-customer', ['as' => 'postExportCsvCustomer', 'uses' => 'CustomerController@exportCsv']);
    //Import CSV
    Route::post('/import-csv-customer', ['as' => 'postImportCsvCustomer', 'uses' => 'CustomerController@importCsv']);
    // //Import CSV
    // Route::post('/table-import-csv', ['as' => 'postTableImportCsv', 'uses' => 'CustomerController@tableImportCsv']);
    //Ajax popup add customer
    Route::post('/popup-add-customer', ['as' => 'postPopupAddCustomer', 'uses' => 'CustomerController@popupAddCustomer']);
     //Ajax submit add customer
    Route::post('/submit-add-customer', ['as' => 'postSubmitAddCustomer', 'uses' => 'CustomerController@submitAddCustomer']);

    //Route sản phẩm
	Route::get('/list-product', ['as' => 'getListProduct', 'uses' => 'ProductController@listProduct']);
    //Tìm kiếm sản phẩm Ajax
    Route::post('/search-product', ['as' => 'postSearchProduct', 'uses' => 'ProductController@search']);
    //Hover ID show Image
    Route::post('/hover-show-image-product', ['as' => 'postShowImageProduct', 'uses' => 'ProductController@hoverShowImage']);
    //Xóa sản phẩm
    Route::post('/delete-product', ['as' => 'deleteProduct', 'uses' => 'ProductController@deleteProduct']);
    //Thêm mới sản phẩm
	Route::get('/add-product', ['as' => 'getAddProduct', 'uses' => 'ProductController@addProduct']);
    //Submit add sản phẩm
	Route::post('/submit-add-product', ['as' => 'postSubmitAddProduct', 'uses' => 'ProductController@submitAddProduct']);
    //Thêm mới sản phẩm
	Route::get('/edit-product', ['as' => 'getEditProduct', 'uses' => 'ProductController@editProduct']);
    //Submit Edit sản phẩm
	Route::post('/submit-edit-product', ['as' => 'postSubmitEditProduct', 'uses' => 'ProductController@submitEditProduct']);

    //Router Đơn hàng
    Route::get('/list-order', ['as' => 'getListOrder', 'uses' => 'OrderController@listOrder']);
    //Popup Đơn hàng
    Route::post('/detail-order', ['as' => 'postDetailOrder', 'uses' => 'OrderController@detailOrder']);
});
