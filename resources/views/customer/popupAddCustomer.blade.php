<!-- Login Screen style -->
<link rel="stylesheet" href="../../assets/css/login.css">
<style>
    input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 1000px white inset !important;
    }
</style>
<div class="col-md-12">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">THÊM KHÁCH HÀNG MỚI</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        
        <form id="quickForm" autocomplete="off">
      
        <div class="card-body">
 
            <div id="errorAlert" class="text-danger"></div><br />
            <div class="form-group" id="groupcustomer_name">
                <label for="name">Họ & Tên (*)</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Nhập đầy đủ họ & tên" autocomplete="off">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="groupemail">
                <label for="email">Email (*)</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Nhập Email đúng định dạng" autocomplete="off">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="grouptel_num">
                <label for="password">Điện thoại (*)</label>
                <input class="form-control" id="tel_num" placeholder="Nhập số điện thoại" autocomplete="new-password">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="groupaddress">
                <label for="password">Địa chỉ (*)</label>
                <input class="form-control" id="address" placeholder="Nhập địa chỉ" autocomplete="new-password">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-check" id="groupactive">
                <input type="checkbox" checked="" class="active form-check-input" id="active">
                <label class="form-check-label" for="active">Check trạng thái hoạt động</label>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div onclick="submitAddCustomer();" class="col-3 btn btn-primary pull-right" style="float:right;margin-left:10px">Lưu</div> 
            <div onclick="closePopup();" class="col-3 btn btn-secondary" style="float:right">Hủy</div>
        </div>
        </form>
    </div>
</div>

<script>
    function submitAddCustomer(){
        customer_name = document.getElementById("name").value;
        email = document.getElementById("email").value;
        tel_num = document.getElementById("tel_num").value;
        address = document.getElementById("address").value;
        active = $('#active').is(":checked") ? 1 : 0;
        
        //refresh field error
        $(".form-group").find(".form-control").removeClass("is-invalid");
        $(".form-group").find(".invalid-feedback").html('');
        $.ajax({
            type: "post",
            url: APP_URL + '/submit-add-customer',
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},	
            data:{'customer_name': customer_name, 'email': email, 'tel_num': tel_num, 'address': address, 'active': active} ,			  
            success: function(data){
                response = $.parseJSON(data);
                if(response['errorCode'] == 0){
                    closePopup();
                    var titleAlert = "Thành công !";
                }else{
                    var titleAlert = "Có lỗi xảy ra !";
                }

                Swal.fire(
                titleAlert,
                response['message'],
                response['status']
                );

            },
            error: function(data){
                //Mã lỗi validation của Laravel
                if(data.status == '422'){
                    //@param json
                    response = JSON.parse(data['responseText']);
                    $.each( response.errors, function( key, value ) {
                        $("#group"+key).find(".form-control").addClass("is-invalid");
                        $("#group"+key).find(".invalid-feedback").html(value);
                    });
                }else{
                    alert("Lỗi không xác định");return false;
                }
                
            }
        });
    }
</script>