@extends('layouts.admin')

@section('title', 'Danh sách khách hàng')

@section('breadcrumbNav')
  @parent
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i> Home</a></li>
      <li class="breadcrumb-item"><a href="#">Customer</a></li>
      <li class="breadcrumb-item active">List</li>
    </ol>
  </div>
@endsection

@section('content')
  <div class="card-header">
    <div class="row">
      <div class="form-group col-md-3 col-sm-12">
          <label>Họ & Tên</label>
          <input value="{{ app('request')->input('nameLike') }}" id="nameSearch" type="text" class="form-control" placeholder="Nhập họ tên" autocomplete="off">
      </div>
      
      <div class="form-group col-md-3 col-sm-12">
          <label>Email</label>
          <input value="{{ app('request')->input('emailLike') }}" id="emailSearch" type="text" class="form-control" placeholder="Nhập email" autocomplete="off">
      </div>
      
      <div class="form-group col-md-3 col-sm-12">
          <label for="exampleSelectRounded0">Trạng thái</label>
          <select class="custom-select rounded-0" id="statusSearch" autocomplete="off">
              <option value="" disabled selected="">Chọn trạng thái</option>
              <option {{ (app('request')->input('isActive')) == '1' ? 'selected': '' }} value="1">Đang hoạt động</option>
              <option {{ (app('request')->input('isActive')) == '0' ? 'selected': '' }} value="0">Tạm khóa</option>
          </select>
      </div>
      
      <div class="form-group col-md-3 col-sm-12">
          <label>Địa chỉ</label>
          <input value="{{ app('request')->input('addressLike') }}" id="addressSearch" type="text" class="form-control" placeholder="Nhập địa chỉ" autocomplete="off">
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div onclick="showPopupAdd();" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-user-plus"></i> Thêm mới</div>
            <input accept=".csv" name="uploadCsv" style="display:none" id="uploadCsv" type="file" autocomplete="off" onchange="importCsvSubmit();" autocomplete="off" />
            <label for="uploadCsv" style="font-weight:normal" class="btn btn-sm-block bg-gradient-success btn-sm col-md-3"><i class="fas fa-upload"></i> Import CSV</label>
            <div onclick="exportCsv();" class="btn btn-sm-block bg-gradient-success btn-sm col-md-3"><i class="fas fa-download"></i> Export CSV</div>
        </div>

        <div class="col-md-6" style="text-align:right">
            <button onclick="search();" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-search"></i> Tìm kiếm</button>
            <button onclick="clearSearchField();" class="btn btn-sm-block bg-gradient-success btn-sm col-md-3"><i class="fas fa-times"></i> Xóa tìm kiếm</button>
        </div>
        
    </div>
  </div>
  <div id="cardBody" class="card-body">
    @include('/customer/dataTableCustomers')
  </div>
@endsection

@section('scriptProcess')
  <script>
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
          search();
        }
    });
  
    function ajaxPaginate(page)
    {
      nameSearch = document.getElementById("nameSearch").value;
      emailSearch = document.getElementById("emailSearch").value;
      addressSearch = document.getElementById("addressSearch").value;
      statusSearch = document.getElementById("statusSearch").value;

      pathName = window.location.pathname;
      
      //Phân trang bình thường
      if(nameSearch == '' && emailSearch == '' && addressSearch == '' && statusSearch == ''){
        urlAjax = '?page=' + page;
      //Trường hợp phân trang với điều kiện search  
      }else{
        urlAjax = '?nameLike='+nameSearch+'&emailLike='+emailSearch+'&addressLike='+addressSearch+'&isActive='+statusSearch+'&page=' + page;
      }
      stateUrl = pathName + urlAjax;

      $.ajax({
        type: "get",
        data: {paginate: 1},
        url: urlAjax,	  
        beforeSend: function() {
          $("#bodyTable tr").hide();
          $("#loadingGif").show();
        },
        success: function(data){
          window.history.pushState(null,'',stateUrl);
          $("#loadingGif").hide();
          $("#cardBody").html(data);
        },
      });
    }

    function search(){
      nameSearch = document.getElementById("nameSearch").value;
      emailSearch = document.getElementById("emailSearch").value;
      statusSearch = document.getElementById("statusSearch").value;
      addressSearch = document.getElementById("addressSearch").value;
    
      if(nameSearch == "" && emailSearch == "" && addressSearch == "" && statusSearch == ""){
        Swal.fire(
          'Thiếu thông tin',
          'Vui lòng điền ít nhất 1 trường thông tin rồi thử lại !',
          'info'
        );
        return false;
      }else{
        
        submitSearch(nameSearch, emailSearch, addressSearch, statusSearch);
      }
    }

    function submitSearch(nameSearch, emailSearch, addressSearch, statusSearch){
      pathName = window.location.pathname;
      $.ajax({
        type: "post",
        url: APP_URL + '/search-customer',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{'nameLike': nameSearch, 'emailLike': emailSearch, 'addressLike': addressSearch, 'isActive': statusSearch},
        beforeSend: function() {
          $("#bodyTable tr").hide();
          $("#loadingGif").show();
        },			  
        success: function(data){
            if(nameSearch == '' && emailSearch == '' && addressSearch == '' && statusSearch == ''){
              window.history.pushState(null,'',pathName);
            }else{
              window.history.pushState(null,'',pathName+'?nameLike='+nameSearch+'&emailLike='+emailSearch+'&addressLike='+addressSearch+'&isActive='+statusSearch);
            }
            $("#loadingGif").hide();
            $("#cardBody").html(data);      
        },
        error: function(){}
      });
    }

    function clearSearchField(){
      $("#nameSearch").val('');
      $("#emailSearch").val('');
      $("#addressSearch").val('');
      $("#statusSearch").val('');
      submitSearch('','','','');
    }

    function exportCsv(){
      nameSearch = document.getElementById("nameSearch").value;   
      emailSearch = document.getElementById("emailSearch").value;
      statusSearch = document.getElementById("statusSearch").value;
      addressSearch = document.getElementById("addressSearch").value;
      url = '{{ route("postExportCsvCustomer") }}'+'?nameLike='+nameSearch+'&emailLike='+emailSearch+'&addressLike='+addressSearch+'&isActive='+statusSearch;
      window.location.href = url;
    }

    function importCsvSubmit(){
      var form_data = new FormData();
      attachment_data= $("#uploadCsv")[0].files[0];
      form_data.append("attachment", attachment_data);
      $.ajax({
        type: "POST",
        data: form_data,
        url: APP_URL + '/import-csv-customer',
        contentType: false,
        processData: false,
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function (data) {
            $('#popup_all_to').html(data);
            $('#popup_all_to').popup('show');
            $('#uploadCsv').val('');
        },
        error: function (data) {
          alert("Có lỗi xảy ra khi gửi request");
          // //alert("loi")
          // //alert(data.status);return false;
          // //alert(data.responseText);return false;
          // response = JSON.parse(data['responseText']);
          // alert(response.message);return false;
        }
      });
    }

    function showPopupAdd(){
      
      // html = '<p style="background:white">Popup thêm khách hàng</p>';
      // $('#popup_all_to').html(html);
      // $('#popup_all_to').popup('show');
      $.ajax({
        type: "post",
        url: APP_URL + '/popup-add-customer',
        data:{} ,
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},			  
        success: function(data){
            //alert(data); 
            $('#popup_all_to').html(data);
            $('#popup_all_to').popup('show');
        },
        error: function(){}
      });
    }

    function editRow(idCustomer){
      customer_name = $("#customer_"+idCustomer).find(".customer_name").text();
      email = $("#customer_"+idCustomer).find(".email").text();
      address = $("#customer_"+idCustomer).find(".address").text();
      tel_num = $("#customer_"+idCustomer).find(".tel_num").text();
      //alert(customer_name);
      $("#customer_"+idCustomer).find(".customer_name").html('<input class="form-control" id="customer_name_update_'+idCustomer+'" value="'+customer_name+'" autocomplete="off" /><span class="error invalid-feedback"></span>');
      $("#customer_"+idCustomer).find(".email").html('<input class="form-control" id="email_update_'+idCustomer+'" value="'+email+'" autocomplete="off" /><span class="error invalid-feedback"></span>');
      $("#customer_"+idCustomer).find(".address").html('<textarea class="form-control" id="address_update_'+idCustomer+'" autocomplete="off">'+address+'</textarea><span class="error invalid-feedback"></span>');
      $("#customer_"+idCustomer).find(".tel_num").html('<input class="form-control" id="tel_num_update_'+idCustomer+'" value="'+tel_num+'" autocomplete="off" /><span class="error invalid-feedback"></span>');

      $("#customer_"+idCustomer).find(".listFunctionInDataTable").html('<i onclick="submitEditCustomer('+idCustomer+');" style="color:#1e7e34" class="fas fa-save"></i>');
    }

    function submitEditCustomer(idCustomer){
      customer_name_update = document.getElementById("customer_name_update_"+idCustomer).value;
      email_update = document.getElementById("email_update_"+idCustomer).value;
      address_update = document.getElementById("address_update_"+idCustomer).value;
      tel_num_update = document.getElementById("tel_num_update_"+idCustomer).value;

      $("#customer_"+idCustomer).find(".form-control").removeClass("is-invalid");
      $("#customer_"+idCustomer).find(".invalid-feedback").html('');
      
      $.ajax({
        type: "post",
        url: APP_URL + '/submit-edit-customer',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{'customer_id': idCustomer, 'customer_name': customer_name_update, 'email': email_update, 'address': address_update, 'tel_num': tel_num_update} ,			  
        success: function(data){
          // alert(data);return false;
          response = $.parseJSON(data);
          //Nếu có thay đổi email và unique lỗi
          if(response['errorCode'] == 2){
              $("#customer_"+idCustomer+" .email").find(".form-control").addClass("is-invalid");
              $("#customer_"+idCustomer+" .email").find(".invalid-feedback").html(response['message']);
          }else{
            if(response['errorCode'] == 0){
              updateNewInfo(idCustomer, customer_name_update, email_update, address_update, tel_num_update);
              var titleAlert = "Thành công !";  
            }else{
              var titleAlert = "Có lỗi xảy ra !";
            }

            Swal.fire(
            titleAlert,
            response['message'],
            response['status']
            );
          }
          
        },
        error: function(data){
          //Mã lỗi validation của Laravel
          if(data.status == '422'){
              //@param json
              response = JSON.parse(data['responseText']);
              $.each( response.errors, function( key, value ) {
                  //alert(key+" : "+value);return false;
                  $("#customer_"+idCustomer+" ."+key).find(".form-control").addClass("is-invalid");
                  $("#customer_"+idCustomer+" ."+key).find(".invalid-feedback").html(value);

                  // $("#group"+key).find(".form-control").addClass("is-invalid");
                  // $("#group"+key).find(".invalid-feedback").html(value);
              });
          }else{
              alert("Lỗi không xác định");return false;
          }
        }
      });
    }

    function submitEditCustomer_old(idCustomer){
      return false;
      customer_name_update = document.getElementById("customer_name_update_"+idCustomer).value;
      email_update = document.getElementById("email_update_"+idCustomer).value;
      address_update = document.getElementById("address_update_"+idCustomer).value;
      tel_num_update = document.getElementById("tel_num_update_"+idCustomer).value;
      
      $("#customer_"+idCustomer+" .form-group").find(".form-control").removeClass("is-invalid");
      $("#customer_"+idCustomer+" .form-group").find(".invalid-feedback").html('');

      $.ajax({
        type: "post",
        url: APP_URL + '/submit-edit-customer',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{'customer_id': idCustomer, 'customer_name': customer_name_update, 'email': email_update, 'address': address_update, 'tel_num': tel_num_update} ,			  
        success: function(data){
          response = $.parseJSON(data);
          if(response['errorCode'] == 2){
            $.each( response['message'], function( key, value ) {
              $("#customer_"+idCustomer+" ."+key).find(".form-control").addClass("is-invalid");
              $("#customer_"+idCustomer+" ."+key).find(".invalid-feedback").html(value);
            });
            Swal.fire(
              'Opp....',
              'Thông tin chỉnh sửa không hợp lệ',
              'error'
            );
            return false;
          }else if(response['errorCode'] == 0){
            closePopup();
            //update lại table với dữ liệu mới
            updateNewInfo(idCustomer, customer_name_update, email_update, address_update, tel_num_update);
            var titleAlert = "Thành công !";
          }else{
            var titleAlert = "Có lỗi xảy ra !";
          }

          Swal.fire(
            titleAlert,
            response['message'],
            response['status']
          );
        },
        error: function(){}
      });
    }

    function updateNewInfo(customer_id, customer_name, email, address,tel_num){
      $("#customer_"+customer_id).find(".customer_name").html(customer_name);
      $("#customer_"+customer_id).find(".email").html(email);
      $("#customer_"+customer_id).find(".address").html(address);
      $("#customer_"+customer_id).find(".tel_num").html(tel_num);
      $("#customer_"+customer_id).find(".listFunctionInDataTable").html('<i onclick="editRow('+customer_id+');" style="color:#0056b3" class="fas fa-pen"></i>');
    }

  </script>
@endsection