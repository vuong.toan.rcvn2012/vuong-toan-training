@if(count($arrData['customers']) > 0)
<div class="row">
  <div class="list-page-link col-md-12 col-lg-7">{{ $arrData['customers']->onEachSide(2)->links() }}</div>
  <div style="text-align: right;" class="col-sm-12 col-lg-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Hiển thị từ {{ $arrData['customers']->firstItem() }} ~ {{ $arrData['customers']->lastItem() }} trong tổng số {{ $arrData['customers']->total() }} user</div></div>
</div>
<div class="row">
    <table id="tableStyle" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Họ tên</th>
                <th>Email</th>
                <th>Địa chỉ</th>
                <th>Điện thoại</th> 
                <th></th>
            </tr>
        </thead>
        <tbody id="bodyTable">
          <tr id="loadingGif"><td colspan="6"><div class="containerLoading"><img height="80" src="{{ URL::asset('assets/gifs/loading.gif') }}" /></div></td></tr>
          @foreach($arrData['customers'] as $index=>$customer)
            <tr id="customer_{{ $customer->customer_id }}">
                <td>{{ $index+1 }}</td>
                <td title="create At: {{ $customer->created_at }}" class="customer_name">{{ $customer->customer_name }}</td>
                <td class="email">{{ $customer->email }}</td>
                <td class="address">{{ $customer->address }}</td>
                <td class="tel_num">{{ $customer->tel_num }}</td>
                <td class="listFunctionInDataTable">
                    <i onclick="editRow('{{ $customer->customer_id }}')" style="color:#0056b3" class="fas fa-pen"></i>
                </td>
            </tr>
          @endforeach
        </tbody>
    </table>
</div>
<div class="row col-12"><div class="list-page-link">{{ $arrData['customers']->onEachSide(2)->links() }}</div></div>

@include('scriptUnbindPaginate')
@else
<tbody><tr><td>Không có dữ liệu hiển thị</td></tr></tbody>
@endif