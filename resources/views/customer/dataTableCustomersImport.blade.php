<div id="cardBody" class="card-body" style="background:#fff">
@if(count($dataCsv) > 0)
<div class="row">
    <table id="tableStyle" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Họ tên</th>
                <th>Email</th>
                <th>Địa chỉ</th>
                <th>Điện thoại</th> 
                <th>Trạng thái</th>
            </tr>
        </thead>
        <tbody id="bodyTable">
          @foreach($dataCsv as $key=>$customer)
            <tr>
                <td>{{ $key+1 }}</td>
                <td class="customer_name">{{ $customer[0] }}</td>
                <td class="email">{{ $customer[1] }}</td>
                <td class="address">{{ $customer[2] }}</td>
                <td class="tel_num">{{ $customer[3] }}</td>
                <td class="listFunctionInDataTable">{!!html_entity_decode($customer[4])!!}</td>
            </tr>
          @endforeach
        </tbody>
    </table>
</div>

@else
<tbody><tr><td>Cấu trúc file upload không đúng file mẫu.</td></tr></tbody>
@endif
</div>
