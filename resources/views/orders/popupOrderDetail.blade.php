<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title" style="line-height: 30px">Danh sách sản phẩm trong đơn hàng</h3>
        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 180px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Tìm mã sản phẩm">
            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 300px;">
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th style="text-align:center">Detail Line</th>
              <th style="text-align:center">Mã SP</th>
              <th style="text-align:center">Giá</th>
              <th style="text-align:center">Số lượng</th>
              <th style="text-align:center">Mã Shop</th>
              <th style="text-align:center">Mã người nhận</th>
            </tr>
          </thead>
          <tbody>
            @for($i=10;$i<18;$i++)
              <tr>
                <td align="center">{{ $i-9 }}</td>
                <td align="center">SP000{{ $i }}</td>
                <td align="center">{{ number_format(rand(10,100)*1000, 0) }} vnđ</td>
                <td align="center">{{ rand(1, 3) }}</td>
                <td align="center">SHOP00{{ rand(10, 99) }}</td>
                <td align="center">KH00XS01{{ rand(10, 99) }}</td>
              </tr>
            @endfor
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <blockquote style="text-align:right;">
      <div>Tổng tiền sản phẩm: 1,000,000 vnđ</div>
      <div>Phí ship: 100,000 vnđ</div>
      <div>Thuế VAT: 100,000 vnđ</div>
      <div>Doanh thu: <strong class="text-danger" style="font-size: 20pt;">800,000 vnđ</strong></div>
      
      
    </blockquote>
    <!-- /.card -->
  </div>
</div>