@extends('layouts.admin')

@section('styles')
<style>
  .card-main{
    background: none;
    box-shadow: none;
  }

  #tableStyle{
    background: #fff;
  }

  .box-search-left{
      background: #fff;
      box-shadow: 0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);
      padding:20px 10px;
  }

  .custom-radio{
    margin:3px 0;
  }

  .card-body-main{
    padding:0;
  }
</style>
@endsection

@section('title', 'Danh sách đơn hàng')

@section('breadcrumbNav')
@parent
<div class="col-sm-6">
  <ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="#">Order</a></li>
    <li class="breadcrumb-item active">List</li>
  </ol>
</div><!-- /.col -->
@endsection

@section('content')
<div class="row card-body card-body-main">
  <div class="box-search-left col-md-4 col-lg-4">

    {{-- Tìm theo mã đơn hàng hoặc mã khách hàng --}}
    <label class="text-info" for="exampleSelectRounded0">Tìm kiếm theo mã</label>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-search"></i></span>
      </div>
      <input type="email" class="form-control" placeholder="Nhập mã ĐH hoặc mã KH" autocomplete="off" /> 
    </div>

    {{-- Theo shop bán hàng --}}
    <div class="form-group">
      <label class="text-info" for="exampleSelectRounded0">Lọc theo shop bán hàng</label>
      <select class="custom-select rounded-0" id="exampleSelectRounded0" autocomplete="off">
        <option selected= "">Tất cả</option>
        <option>Văn phòng ADC (Asia Development Center)</option>
        <option>Văn phòng SSC (Saigon Solution Center)</option>
      </select>
    </div>

    {{-- Phương thức thanh toán --}}
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title">Phương thức thanh toán</h3>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-4 custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="option1">
            <label for="customCheckbox1" class="custom-control-label"><span style="font-weight:normal">COD</span></label>
          </div>
          <div class="col-4 custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" id="customCheckbox2" value="option1">
            <label for="customCheckbox2" class="custom-control-label"><span style="font-weight:normal">Paypal</span></label>
          </div>
          <div class="col-4 custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="option1">
            <label for="customCheckbox3" class="custom-control-label"><span style="font-weight:normal">GMO</span></label>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>

    {{-- Theo trị giá đơn hàng --}}
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title">Giá trị đơn hàng</h3>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Từ</label>
          <div class="col-sm-10">
            <input class="form-control" id="inputEmail3" placeholder="Nhập giá thấp nhất" autocomplete="off">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail4" class="col-sm-2 col-form-label">Đến</label>
          <div class="col-sm-10">
            <input class="form-control" id="inputEmail4" placeholder="Nhập giá cao nhất" autocomplete="off">
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>

    {{-- Theo ngày --}}
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title">Lọc theo ngày</h3>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>Ngày đặt hàng: Từ... Đến... :</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="far fa-calendar-alt"></i>
              </span>
            </div>
            <input type="text" class="form-control float-right" id="ngayDatHang" autocomplete="off">
          </div>
          <!-- /.input group -->
        </div>

        <div class="form-group">
          <label>Ngày giao hàng: Từ... Đến... :</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="far fa-calendar-alt"></i>
              </span>
            </div>
            <input type="text" class="form-control float-right" id="ngayGiaoHang" autocomplete="off">
          </div>
          <!-- /.input group -->
        </div>
      </div>
      <!-- /.card-body -->
    </div>

    {{-- Theo trạng thái đơn --}}
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title">Trạng thái đơn hàng</h3>
      </div>
      <div class="card-body">
        <div class="form-group">
          <div class="custom-control custom-radio">
            <input checked class="custom-control-input custom-control-input-primary custom-control-input-outline" type="radio" id="customRadio" name="customRadio2" autocomplete="off">
            <label for="customRadio" class="custom-control-label"><span style="font-weight:normal">Tất cả đơn hàng</span></label>
          </div>
          <div class="custom-control custom-radio">
            <input class="custom-control-input custom-control-input-warning custom-control-input-outline" type="radio" id="customRadio1" name="customRadio2" autocomplete="off">
            <label for="customRadio1" class="custom-control-label"><span style="font-weight:normal">Đơn hàng ĐANG GIAO</label>
          </div>
          <div class="custom-control custom-radio">
            <input class="custom-control-input custom-control-input-success custom-control-input-outline" type="radio" id="customRadio2" name="customRadio2" autocomplete="off">
            <label for="customRadio2" class="custom-control-label"><span style="font-weight:normal">Đơn hàng ĐÃ GIAO</label>
          </div>
          <div class="custom-control custom-radio">
            <input class="custom-control-input custom-control-input-danger custom-control-input-outline" type="radio" id="customRadio3" name="customRadio2" autocomplete="off">
            <label for="customRadio3" class="custom-control-label"><span style="font-weight:normal">Đơn hàng ĐÃ HỦY</label>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    

  </div>
  <div class="dataTable col-md-8 col-lg-8">
    <div class="row">
      <div class="col-sm-12">
      <table id="tableStyle" class="table table-bordered table-striped">
          <thead>
              <tr>
                  <th style="text-align:center">Mã ĐH</th>
                  <th style="text-align:center">Mã KH</th>
                  <th>Ngày đặt</th>
                  <th>Tổng ĐH</th>
                  <th>Trạng thái</th>
                  <th style="text-align:center">Chi tiết</th> 
              </tr>
          </thead>
          <tbody>
            @for($i=10;$i<35;$i++)
              <tr>
                <td align="center">HD00015{{ $i }}</td>
                <td align="center">KH2021{{ $i }}</td>
                <td>2021-07-{{ $i-5 }} 16:14:50</td>
                <td>{{ number_format(rand(100,1000)*1000, 0) }} vnđ</td>
                <td>
                  @if($i == 10 || $i == 11 || $i == 12 || $i == 15 || $i == 16)
                    <span class="text-warning">Đang giao hàng</span>
                  @elseif ($i == 14 || $i == 18 || $i == 20)
                  <span class="text-danger">Đã HỦY</span>
                  @else
                    <span class="text-success">Đã giao hàng</span>
                  @endif
                </td>
                {{-- <td align="center"><a target="_blank" href="{{ route('getDetailOrder').'?orderId=HD00015'.$i }}"><i class="fas fa-info-circle"></i></a></td> --}}
                <td align="center"><div title="Xem chi tiết đơn hàng HD00015{{ $i }}" style="cursor: pointer" onclick="popupDetailOrder('HD00015{{ $i }}');"><i class="fas fa-info-circle"></i></div></td>
              </tr>
            @endfor
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scriptProcess')
  <!-- date-range-picker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <!-- date-range-picker -->
  <script src="{{ URL::asset('assets/adminTLE3/plugins/daterangepicker/daterangepicker.js') }}"></script>
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/daterangepicker/daterangepicker.css') }}">

  <script>
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
          alert("Chạy bộ lọc tìm kiếm");
        }
    });
    //Date range picker
    $('#ngayDatHang').daterangepicker();
    $('#ngayGiaoHang').daterangepicker();

    function popupDetailOrder(orderId){
      $.ajax({
        type: "post",
        url: '{{ route('postDetailOrder') }}',
        data:{'orderId': orderId} ,
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},			  
        success: function(data){
            //alert(data); 
            $('#popup_all_to').html(data);
            $('#popup_all_to').popup('show');
        },
        error: function(){}
      });
    }
  </script>

  
@endsection

