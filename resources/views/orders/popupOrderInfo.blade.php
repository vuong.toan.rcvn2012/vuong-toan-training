<div class="row">
  <div class="col-md-12">
    <!-- The time line -->
    <div class="timeline">
      <!-- timeline time label -->
      <div class="time-label">
        <span class="bg-red">17-07-2021</span>
      </div>
      <!-- /.timeline-label -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-motorcycle bg-yellow"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 11:30</span>
          <h3 class="timeline-header"><a href="#">Shipper</a> đang trả hàng về kho</h3>
        </div>
      </div>
      <!-- timeline time label -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-times-circle bg-red"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 11h:00</span>
          <h3 class="timeline-header no-border"><a href="#">Vương Toàn (KH202110)</a> gửi yêu cầu <b class="text-danger">HỦY ĐƠN HÀNG</b> do khu vực bị cách ly.</h3>
        </div>
      </div>
      <!-- END timeline item -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-motorcycle bg-yellow"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 08:30</span>
          <h3 class="timeline-header"><a href="#">Shipper</a> đang giao hàng đến khách</h3>
          <div class="timeline-body">
            Thời gian dự kiến giao cho khách khoảng: 17h đến 18h cùng ngày.
          </div>
          {{-- <div class="timeline-footer">
            <a class="btn btn-primary btn-sm">Read more</a>
            <a class="btn btn-danger btn-sm">Delete</a>
          </div> --}}
        </div>
      </div>
      <!-- timeline time label -->
      <div class="time-label">
        <span class="bg-green">17-07-2021</span>
      </div>
      <!-- /.timeline-label -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-truck bg-yellow"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 15:05</span>
          <h3 class="timeline-header"><a href="#">Vận chuyển</a> đã nhập kho</h3>
          <div class="timeline-body">
            Thời gian dự kiến giao cho khách khoảng: ngày 17-07-2021.
          </div>
          {{-- <div class="timeline-footer">
            <a class="btn btn-primary btn-sm">Read more</a>
            <a class="btn btn-danger btn-sm">Delete</a>
          </div> --}}
        </div>
      </div>
      <!-- END timeline item -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-check-circle bg-green"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 09h:15</span>
          <h3 class="timeline-header no-border"><a href="#">Nhân viên C</a> xác nhận đủ số lượng & chuyển sang bộ phận giao hàng.</h3>
        </div>
      </div>
      <!-- END timeline item -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-check-circle bg-green"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 08h:00</span>
          <h3 class="timeline-header"><a href="#">Nhân viên B</a> đã đóng gói đơn hàng & chuyển sang bộ phận kiểm tra.</h3>
          <div class="timeline-body">
            <u>Ghi chú:</u> Ưu tiên giao sớm cho khách, vận chuyển cẩn thận tránh hư hàng.
          </div>
          {{-- <div class="timeline-footer">
            <a class="btn btn-warning btn-sm">View comment</a>
          </div> --}}
        </div>
      </div>
      <!-- END timeline item -->
      <!-- timeline time label -->
      <div class="time-label">
        <span class="bg-green">16-07-2021</span>
      </div>
      <!-- /.timeline-label -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-check-circle bg-green"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 17h:00</span>
          <h3 class="timeline-header no-border"><a href="#">Nhân viên A</a> xác nhận đơn hàng & chuyển trạng thái chuẩn bị</h3>
        </div>
      </div>
      <!-- END timeline item -->
      <!-- timeline item -->
      <div>
        <i class="fas fa-user bg-yellow"></i>
        <div class="timeline-item">
          <span class="time"><i class="fas fa-clock"></i> 16h:14p</span>
          <h3 class="timeline-header"><a href="#">Vương Toàn (KH202110)</a> đã đặt 1 đơn hàng COD của shop <strong class="text-info">Saigon Solution Center (SHOP0094)</strong></h3>
          <div class="timeline-body">
            <u>Ghi chú khách hàng:</u> Vui lòng giao hàng sớm vì cần gấp, hàng date còn xa và đóng gói cẩn thận không móp méo khi vận chuyển.
          </div>
        </div>
      </div>
      <!-- END timeline item -->
      <div>
        <i class="fas fa-clock bg-gray"></i>
      </div>
    </div>
  </div>
  <!-- /.col -->
</div>