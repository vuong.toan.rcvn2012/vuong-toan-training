@extends('layouts.admin')

@section('title', 'Sửa sản phẩm')

@section('breadcrumbNav')
@parent
<div class="col-sm-6">
  <ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="#">Product</a></li>
    <li class="breadcrumb-item active">Edit</li>
  </ol>
</div><!-- /.col -->
@endsection

@section('content')
<form action="{{ route('postSubmitEditProduct') }}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}
  <input type="hidden" value="{{ $arrData['product']['product_id'] }}" id="product_id" name="product_id" />
<div id="cardBody" class="card-body">
  @if(session('status'))
    <ul class="listError">
        <li class="text-danger"> {{ session('status') }}</li>
    </ul>
  @endif
  <div class="row">
    <div class="col-md-6">
      <div class="form-group" id="groupname">
          <label for="name">Tên sản phẩm (*)</label>
          <input value="{{ old('product_name', $arrData['product']['product_name']) }}" type="text" name="product_name" class="form-control {{ $errors->first('product_name') ? 'is-invalid':'' }}" id="product_name" placeholder="Nhập tên sản phẩm" autocomplete="off">
          @error('product_name')
            <span class="error invalid-feedback">{{ $message }}</span>
          @enderror
      </div>
      <div class="form-group" id="groupemail">
          <label for="email">Giá bán (*)</label>
          <input value="{{ old('product_price', number_format($arrData['product']['product_price'], 0, ",","")) }}" type="text" name="product_price" class="form-control {{ $errors->first('product_price') ? 'is-invalid':'' }}" id="product_price" placeholder="Nhập giá bán" autocomplete="off">
          @error('product_price')
            <span class="error invalid-feedback">{{ $message }}</span>
          @enderror
      </div>
      <div class="form-group" id="groupdescription">
        <label>Mô tả</label>
        <textarea id="description" name="description" class="form-control" rows="3" placeholder="Mô tả sản phẩm">{{ old('description', $arrData['product']['description']) }}</textarea>
        <span class="error invalid-feedback"></span>
      </div>
      <div class="form-group" id="groupisSales">
        <label for="exampleSelectRounded0">Trạng thái (*)</label>
        <select class="group form-control custom-select rounded-0 {{ $errors->first('is_sales') ? 'is-invalid':'' }}" id="is_sales" name="is_sales" autocomplete="off">
            <option value ="" selected="" disabled>Chọn trạng thái</option>
            <option {{ old('is_sales', $arrData['product']['is_sales']) == 0 ? 'selected':'' }} value="0">Dừng bán hoặc dừng sản xuất</option>
            <option {{ old('is_sales', $arrData['product']['is_sales']) == 1 ? 'selected':'' }} value="1">Có hàng bán</option>
        </select>
        @error('is_sales')
          <span class="error invalid-feedback">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div style="margin:auto" id="imageShow">
          @if(old('product_image_tmp'))
            <label for="inputUploadImage"><span class="item_image" style="background-image:url({{ old('product_image_tmp') }})"></span></label>
          @else
            @if($arrData['product']['product_image'] != "" && old('flag_delete_image') != 1)
              <label for="inputUploadImage"><img style="max-width:100%;" src="{{ $arrData['product']['product_image'] }}" /></label>
            @else
              <label for="inputUploadImage"><i style="font-size:255px" class="fas fa-image"></i></label>
            @endif
          @endif
          
        </div>
      </div>
      <div class="row" style="text-align:center;margin:10px 0;">
        <input type="hidden" value="{{ $errors->first('product_image') ? '' : old('product_image_tmp') }}" id="product_image_tmp" name="product_image_tmp" autocomplete="off" />
        <input type="hidden" value="{{ old('flag_delete_image', 0) }}" id="flag_delete_image" name="flag_delete_image" autocomplete="off" />
        <input accept="image/*" style="display:none;" id="inputUploadImage" class="{{ $errors->first('product_image') ? 'is-invalid':'' }}" name="product_image" type="file" autocomplete="off" />
        @error('product_image')
          <span class="error invalid-feedback">{{ $message }}</span>
        @enderror
      </div>
      <div class="row">
        <div class="btn-group w-100">
          <label style="margin-bottom:0;font-weight:normal;cursor: pointer;" for="inputUploadImage" class="btn btn-success col fileinput-button dz-clickable">
            <i class="fas fa-plus"></i>
            <span>Chọn ảnh</span>
          </label>
          
          <div onclick="removeImageUpload('{{ $arrData['product']['product_image'] }}');" type="reset" class="btn btn-warning col cancel">
            <i class="fas fa-times-circle"></i>
            <span>Xóa ảnh</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card-footer">
  <button type="submit" class="col-2 btn btn-primary pull-right" style="float:right;margin-left:10px">Lưu</button> 
  <a href={{ route('getListProduct') }} class="col-2 btn btn-secondary" style="float:right">Hủy</a>
</div>
</form>
@endsection

@section('scriptProcess')
  <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
  <script type="text/javascript">
      bkLib.onDomLoaded(function() {
          new nicEditor().panelInstance('description');
      }); // Thay thế text area có id là area1 trở thành WYSIWYG editor sử dụng nicEditor
  </script>
  <script>
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
          search();
        }
    });
    $(document).ready(function() {
      $('#inputUploadImage').change(function (e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            if(file.type.split('/')[0] == 'image'){
              $('#imageShow').html('');
              var reader = new FileReader();
              reader.onload = function (e) {
                  var $imageElement = "<label for='inputUploadImage'><span class='item_image' style='background-image:url("+ e.target.result+")'></span></label>";
                  $("#product_image_tmp").val(e.target.result);
                  $('#imageShow').append($imageElement);
              }
              reader.readAsDataURL(file);
            }else{
              $("#inputUploadImage").val('');
              Swal.fire(
                'Bạn chắc chứ?',
                'File upload là '+file.type+' không phải file hình ảnh !',
                'question'
              )
            }
        }
      })
    })

    function removeImageUpload(pathImage){
      // alert(pathImage);return false;
      // if(pathImage == ''){
      //   $("#imageShow").html('<label for="inputUploadImage"><i style="font-size:255px" class="fas fa-image"></i></label>');
      // }else{
      //   $("#imageShow").html('<label for="inputUploadImage"><img style="max-width:100%;" src="'+pathImage+'" /></label>');
      // }
      $("#imageShow").html('<label for="inputUploadImage"><i style="font-size:255px" class="fas fa-image"></i></label>');
      $("#product_image_tmp").val('');
      $("#inputUploadImage").val('');
      $("#flag_delete_image").val('1');
    }
  </script>
@endsection

