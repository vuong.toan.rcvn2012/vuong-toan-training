@if(count($arrData['products']) > 0)
<div class="row">
    <div class="list-page-link col-md-12 col-lg-7">{{ $arrData['products']->onEachSide(2)->links() }}</div>
    <div style="text-align: right;" class="col-sm-12 col-lg-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Hiển thị từ {{ $arrData['products']->firstItem() }} ~ {{ $arrData['products']->lastItem() }} trong tổng số {{ $arrData['products']->total() }} sản phẩm</div></div>
  </div>
  <div class="row">
    <table id="tableStyle" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Mã sản phẩm</th>
                <th>Tên sản phẩm</th>
                <th>Mô tả</th>
                <th>Giá</th>
                <th>Tình trạng</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="bodyTable">
          <tr id="loadingGif"><td colspan="6"><div class="containerLoading"><img height="80" src="{{ URL::asset('assets/gifs/loading.gif') }}" /></div></td></tr>
          @foreach($arrData['products'] as $index=>$products)
            <tr id="product_{{ $products->product_id }}" class="tableProducts">
                <td>{{ $index+1 }}</td>
                <td id="{{ $products->product_id }}" class="product_id"><span onmouseover="mouseoverProductId('{{ $products->product_id }}');" class="overShowImage" style="cursor: pointer">{{ $products->product_id }}</span></td>
                
                <td class="product_name">{{ $products->product_name }}</td>
                <td style="max-width:500px" class="description">{!!html_entity_decode($products->description)!!}</td>
                <td class="product_price">${{ number_format($products->product_price, 0, ",",".") }}</td>
                <td class="is_sales">
                  @if($products->is_sales == 1)
                    <span class="text-success">Đang bán</span>
                  @else
                    <span class="text-danger">Ngừng bán</span>
                  @endif
                </td>
                <td class="listFunctionInDataTable">
                  <i onclick="window.location='{{ url("edit-product?product_id=$products->product_id") }}'" style="color:#0056b3" class="fas fa-pen"></i>
                  <i onclick="deleteProduct('{{ $products->product_id }}','{{ $products->product_name }}')" style="color:red" class="fas fa-trash-alt"></i>
                </td>
            </tr>
          @endforeach
        </tbody>
    </table>
  </div>
  <div class="row col-12"><div class="list-page-link">{{ $arrData['products']->onEachSide(2)->links() }}</div></div>

  @include('scriptUnbindPaginate')
  @else
  <tbody><tr><td>Không có dữ liệu hiển thị</td></tr></tbody>
  @endif