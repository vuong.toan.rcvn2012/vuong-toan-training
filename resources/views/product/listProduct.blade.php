@extends('layouts.admin')

@section('title', 'Danh sách sản phẩm')

@section('breadcrumbNav')
@parent
<div class="col-sm-6">
  <ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="#">Product</a></li>
    <li class="breadcrumb-item active">List</li>
  </ol>
</div><!-- /.col -->
@endsection

@section('content')
  <div class="card-header">
    <div class="row">
      <div class="form-group col-md-3 col-sm-12">
          <label>Tên sản phẩm</label>
          <input value="{{ app('request')->input('nameLike') }}" id="nameSearch" type="text" class="form-control" placeholder="Nhập tên sản phẩm" autocomplete="off">
      </div>
      
      <div class="form-group col-md-3 col-sm-12">
          <label for="exampleSelectRounded0">Trạng thái</label>
          <select class="custom-select rounded-0" id="statusSearch" autocomplete="off">
              <option value="" disabled selected="">Chọn trạng thái</option>
              <option {{ (app('request')->input('isSales')) == '0' ? 'selected': '' }} value="0">Dừng bán hoặc dừng sản xuất</option>
              <option {{ (app('request')->input('isSales')) == '1' ? 'selected': '' }} value="1">Có hàng bán</option>
          </select>
      </div>

      <div class="form-group col-md-3 col-sm-12">
          <label>Giá bán từ</label>
          <input value="{{ app('request')->input('priceFrom') }}" id="priceFrom" type="text" class="form-control" placeholder="Nhập giá" autocomplete="off">
      </div>
      
      
      
      <div class="form-group col-md-3 col-sm-12">
          <label>Giá bán đến</label>
          <input value="{{ app('request')->input('priceTo') }}" id="priceTo" type="text" class="form-control" placeholder="Nhập giá" autocomplete="off">
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('getAddProduct') }}" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-user-plus"></i> Thêm mới</a>
        </div>

        <div class="col-md-6" style="text-align:right">
            <button onclick="search();" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-search"></i> Tìm kiếm</button>
            <button onclick="clearSearchField();" class="btn btn-sm-block bg-gradient-success btn-sm col-md-3"><i class="fas fa-times"></i> Xóa tìm kiếm</button>
        </div>
        
    </div>
  </div>
  <div id="cardBody" class="card-body">
    @include('/product/dataTableProducts')
  </div>
@endsection

@section('scriptProcess')

  <script>
    
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
          search();
        }
    });
  
    function ajaxPaginate(page)
    {
      nameSearch = document.getElementById("nameSearch").value;
      statusSearch = document.getElementById("statusSearch").value;
      priceFrom = document.getElementById("priceFrom").value;
      priceTo = document.getElementById("priceTo").value;

      pathName = window.location.pathname;
      
      //Phân trang bình thường
      if(nameSearch == '' && priceFrom == '' && priceTo == '' && statusSearch == ''){
        urlAjax = '?page=' + page;
      //Trường hợp phân trang với điều kiện search  
      }else{
        urlAjax = '?nameLike='+nameSearch+'&priceFrom='+priceFrom+'&priceTo='+priceTo+'&isSales='+statusSearch+'&page=' + page;
      }
      stateUrl = pathName + urlAjax;

      $.ajax({
        type: "get",
        data: {paginate: 1},
        url: urlAjax,	  
        beforeSend: function() {
          $("#bodyTable tr").hide();
          $("#loadingGif").show();
        },
        success: function(data){
          window.history.pushState(null,'',stateUrl);
          $("#loadingGif").hide();
          $("#cardBody").html(data);
        },
      });
    }

    function search(){
      nameSearch = document.getElementById("nameSearch").value;
      statusSearch = document.getElementById("statusSearch").value;
      priceFrom = document.getElementById("priceFrom").value;
      priceTo = document.getElementById("priceTo").value;
    
      if(nameSearch == "" && statusSearch == "" && priceFrom == "" && priceTo == ""){
        Swal.fire(
          'Thiếu thông tin',
          'Vui lòng điền ít nhất 1 trường thông tin rồi thử lại !',
          'info'
        );
        return false;
      }else{
        
        submitSearch(nameSearch, statusSearch, priceFrom, priceTo);
      }
    }

    function submitSearch(nameSearch, statusSearch, priceFrom, priceTo){
      pathName = window.location.pathname;
      $.ajax({
        type: "post",
        url: APP_URL + '/search-product',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{'nameLike': nameSearch, 'isSales': statusSearch, 'priceFrom': priceFrom, 'priceTo': priceTo},
        beforeSend: function() {
          $("#bodyTable tr").hide();
          $("#loadingGif").show();
        },				  
        success: function(data){
            if(nameSearch == '' && priceFrom == '' && priceTo == '' && statusSearch == ''){
              window.history.pushState(null,'',pathName);
            }else{
              window.history.pushState(null,'',pathName+'?nameLike='+nameSearch+'&priceFrom='+priceFrom+'&priceTo='+priceTo+'&isSales='+statusSearch);
            }
            $("#loadingGif").hide();
            $("#cardBody").html(data);      
        },
        error: function(){}
      });
    }

    function clearSearchField(){
      $("#nameSearch").val('');
      $("#priceFrom").val('');
      $("#priceTo").val('');
      $("#statusSearch").val('');
      submitSearch('','','','');
    }

    function deleteProduct(idProduct, nameProduct){
      Swal.fire({
        title: 'Bạn có chắc chứ?',
        text: 'Xác nhận xóa sản phẩm "'+nameProduct+'" ra khỏi danh sách hiển thị!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Có, xóa ngay!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
              type: "post",
              url: APP_URL + '/delete-product',
              data:{'product_id': idProduct, 'product_name': nameProduct},
              headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},			  
              success: function(data){	
                      response = $.parseJSON(data);
                      Swal.fire({
                        icon: response['status'],
                        title: 'Kết quả...',
                        text: response['message'],
                      });

                      if(response['errorCode'] == 0){
                        $("#product_"+idProduct).remove();
                      }
                                
                  },
                  error: function(){}
          });
        }
      })
    }

    function mouseoverProductId(product_id){
      $.ajax({
        type: "post",
        url: APP_URL + '/hover-show-image-product',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{'product_id': product_id} ,			  
        success: function(data){
          response = JSON.parse(data)
          html = '<span title="response.message" onmouseout="mouseoutProductId(\''+product_id+'\');" class="overShowImage">';
          if(response.errorCode == 0){
            $("#product_"+product_id).find(".product_id").html(html+'<img style="display:block;margin:auto" width="85" src="'+response.message+'" />'+'</span>');
          }else{
            $("#product_"+product_id).find(".product_id").html(html+'<i title="'+response.message+'" style="font-size:85px" class="fas fa-image"></i>'+'</span>');
          }
        },
        error: function(){}
      });
    }

    function mouseoutProductId(product_id){
      html = '<span title="response.message" onmouseover="mouseoverProductId(\''+product_id+'\');" class="overShowImage">'+product_id+'</span>';
      $("#product_"+product_id).find(".product_id").html(html);
    }

  </script>
@endsection

