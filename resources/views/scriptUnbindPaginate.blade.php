<!-- initial jQuery -->
<script src="{{ URL::asset('assets/adminTLE3/plugins/jquery/jquery.min.js') }}"></script>
<script>
  $('.pagination a').unbind('click').on('click', function(e) {
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    ajaxPaginate(page);
  });
</script>