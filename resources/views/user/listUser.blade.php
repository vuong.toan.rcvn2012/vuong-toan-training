@extends('layouts.admin')

@section('title', 'Danh sách user')

@section('breadcrumbNav')
@parent
<div class="col-sm-6">
  <ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="#">User</a></li>
    <li class="breadcrumb-item active">List</li>
  </ol>
</div><!-- /.col -->
@endsection

@section('content')
<div class="card-header">
  <div class="row">
    <div class="form-group col-md-3 col-sm-12">
        <label>Tên</label>
        <input value="{{ app('request')->input('nameLike') }}" id="nameSearch" type="text" class="form-control" placeholder="Nhập họ tên" autocomplete="off">
    </div>
    
    <div class="form-group col-md-3 col-sm-12">
        <label>Email</label>
        <input value="{{ app('request')->input('emailLike') }}" id="emailSearch" type="text" class="form-control" placeholder="Nhập email" autocomplete="off">
    </div>

    <div class="form-group col-md-3 col-sm-12">
        <label for="exampleSelectRounded0">Nhóm</label>
        <select class="custom-select rounded-0" id="groupSearch" autocomplete="off">
          <option value="" disabled="" selected="">Chọn nhóm</option>
            <option {{ (app('request')->input('groupRole')) == "Admin" ? 'selected': '' }} value="Admin">Admin</option>
            <option {{ (app('request')->input('groupRole')) == "Editor" ? 'selected': '' }} value="Editor">Editor</option>
            <option {{ (app('request')->input('groupRole')) == "Reviewer" ? 'selected': '' }} value="Reviewer">Reviewer</option>
        </select>
    </div>
    <div class="form-group col-md-3 col-sm-12">
        <label for="exampleSelectRounded0">Trạng thái</label>
        <select class="custom-select rounded-0" id="statusSearch" autocomplete="off">
            <option value="" disabled selected="">Chọn trạng thái</option>
            <option {{ (app('request')->input('isActive')) == '1' ? 'selected': '' }} value="1">Đang hoạt động</option>
            <option {{ (app('request')->input('isActive')) == '0' ? 'selected': '' }} value="0">Tạm khóa</option>
        </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
        <div onclick="showPopupAddUser()" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-user-plus"></i> Thêm mới</div>
    </div>

    <div class="col-md-6" style="text-align:right">
        <button onclick="searchUser()" class="btn btn-sm-block bg-gradient-primary btn-sm col-md-3"><i class="fas fa-search"></i> Tìm kiếm</button>
        <button onclick="clearSearchField()" class="btn btn-sm-block bg-gradient-success btn-sm col-md-3"><i class="fas fa-times"></i> Xóa tìm kiếm</button>
    </div>
    
  </div>
</div>
<div id="cardBody" class="card-body">
  @include('/user/dataTableUsers')
</div>
@endsection

@section('scriptProcess')
  <script>
  $(document).on('keypress',function(e) {
      if(e.which == 13) {
        searchUser();
      }
  });

  
  function ajaxPaginate(page)
  {
    nameSearch = document.getElementById("nameSearch").value;
    emailSearch = document.getElementById("emailSearch").value;
    groupSearch = document.getElementById("groupSearch").value;
    statusSearch = document.getElementById("statusSearch").value;

    pathName = window.location.pathname;
    
    //Phân trang bình thường
    if(nameSearch == '' && emailSearch == '' && groupSearch == '' && statusSearch == ''){
      urlAjax = '?page=' + page;
      // stateUrl = pathName+'?page=' + page;
    //Trường hợp phân trang với điều kiện search  
    }else{
      urlAjax = '?nameLike='+nameSearch+'&emailLike='+emailSearch+'&groupRole='+groupSearch+'&isActive='+statusSearch+'&page=' + page;
      // stateUrl = pathName+'?nameLike='+nameSearch+'&emailLike='+emailSearch+'&groupRole='+groupSearch+'&isActive='+statusSearch+'&page=' + page;
    }
    stateUrl = pathName + urlAjax;
  
    $.ajax({
      type: "get",
      // url: '?page=' + page,
      data: {paginate: 1},
      url: urlAjax,	  
      beforeSend: function() {
        $("#bodyTable tr").hide();
        $("#loadingGif").show();
      },
      success: function(data){
        window.history.pushState(null,'',stateUrl);
        $("#loadingGif").hide();
        $("#cardBody").html(data);
        
        //$('body').html(data);
        //alert(data)
      },
    });
  }

  function deleteUser(idUser, nameUser){
    Swal.fire({
      title: 'Bạn có chắc chứ?',
      text: 'Xác nhận xóa User "'+nameUser+'" ra khỏi danh sách hiển thị!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Có, tôi muốn!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
            type: "post",
            url: APP_URL + '/delete-user',
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'id': idUser, 'name': nameUser} ,			  
            success: function(data){	
                    response = $.parseJSON(data);
                    Swal.fire({
                      icon: response['status'],
                      title: 'Kết quả...',
                      text: response['message'],
                    });

                    if(response['errorCode'] == 0){
                      $("#user_"+idUser).remove();
                    }
                              
                },
                error: function(){}
        });
      }
    })
  }

  function changeStatusUser(idUser, nameUser, isActiveCurrent){
    Swal.fire({
      title: 'Bạn có chắc chứ?',
      text: 'Xác nhận chuyển trạng thái User "'+nameUser+'"!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Có, Chuyển ngay!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
            type: "post",
            url: APP_URL + '/change-status-user',
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'id': idUser, 'name': nameUser, 'isActiveCurrent': isActiveCurrent} ,			  
            success: function(data){	
                    //alert(data);return false;
                    response = $.parseJSON(data);
                    Swal.fire({
                      icon: response['status'],
                      title: 'Kết quả...',
                      text: response['message'],
                    });

                    if(response['errorCode'] == 0 && isActiveCurrent == 1){
                      $("#user_"+idUser).find('.status').html('<span class="text-danger">Tạm khóa</span>');
                    }else if(response['errorCode'] == 0 && isActiveCurrent == 0){
                      $("#user_"+idUser).find('.status').html('<span class="text-success">Đang hoạt động</span>');
                    }
                              
                },
                error: function(){}
        });
      }
    })
  }

  function searchUser(){
    nameSearch = document.getElementById("nameSearch").value;
    emailSearch = document.getElementById("emailSearch").value;
    groupSearch = document.getElementById("groupSearch").value;
    statusSearch = document.getElementById("statusSearch").value;

    if(nameSearch == "" && emailSearch == "" && groupSearch == "" && statusSearch == ""){
      Swal.fire(
        'Thiếu thông tin',
        'Vui lòng điền ít nhất 1 giá trị để tìm kiếm !',
        'info'
      );
      return false;
    }else{
      submitSearchUser(nameSearch, emailSearch, groupSearch, statusSearch);
    }
  }

  function clearSearchField(){
    $("#nameSearch").val('');
    $("#emailSearch").val('');
    $("#groupSearch").val('');
    $("#statusSearch").val('');
    submitSearchUser('','','','');
  }

  function submitSearchUser(nameSearch, emailSearch, groupSearch, statusSearch){
    pathName = window.location.pathname;
    $.ajax({
      type: "post",
      url: APP_URL + '/search-user',
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data:{'nameSearch': nameSearch, 'emailSearch': emailSearch, 'groupSearch': groupSearch, 'statusSearch': statusSearch},	
      beforeSend: function() {
        $("#bodyTable tr").hide();
        $("#loadingGif").show();
      },		  
      success: function(data){
          if(nameSearch == '' && emailSearch == '' && groupSearch == '' && statusSearch == ''){
            window.history.pushState(null,'',pathName);
          }else{
            window.history.pushState(null,'',pathName+'?nameLike='+nameSearch+'&emailLike='+emailSearch+'&groupRole='+groupSearch+'&isActive='+statusSearch);
          }
          $("#loadingGif").hide();
          $("#cardBody").html(data);      
      },
      error: function(){}
    });
  }

  function showPopupAddUser(){
      $.ajax({
        type: "post",
        url: APP_URL + '/popup-add-user',
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:{} ,			  
        success: function(data){
            //alert(data); 
            $('#popup_all_to').html(data);
            $('#popup_all_to').popup('show');
        },
        error: function(){}
      });
  }

  function submitAddUser(){

    nameUser = document.getElementById("name").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    repassword = document.getElementById("repassword").value;
    group = document.getElementById("group").value;
    active = $('#active').is(":checked") ? 1 : 0;
    
    //refresh field error
    $(".form-group").find(".form-control").removeClass("is-invalid");
    $(".form-group").find(".invalid-feedback").html('');
    $.ajax({
      type: "post",
      url: APP_URL + '/submit-add-user',
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data:{'name': nameUser, 'email': email, 'password': password, 'repassword': repassword, 'group': group, 'active': active} ,			  
      success: function(data){
          response = $.parseJSON(data);
          if(response['errorCode'] == 0){
              closePopup();
              var titleAlert = "Thành công !";
          }else{
              var titleAlert = "Có lỗi xảy ra !";
          }

          Swal.fire(
          titleAlert,
          response['message'],
          response['status']
          );

      },
      error: function(data){
        //Mã lỗi validation của Laravel
        if(data.status == '422'){
            //@param json
            response = JSON.parse(data['responseText']);
            $.each( response.errors, function( key, value ) {
                $("#group"+key).find(".form-control").addClass("is-invalid");
                $("#group"+key).find(".invalid-feedback").html(value);
            });
        }else{
            alert("Lỗi không xác định");return false;
        }
      }
    });

  }

  function showPopupEditUser(idUser, csrfToken){
    
    $.ajax({
      type: "post",
      url: APP_URL + '/popup-edit-user',
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data:{'idUser': idUser} ,			  
      success: function(data){
          //alert(data); return false;
          $('#popup_all_to').html(data);
          $('#popup_all_to').popup('show');
      },
      error: function(){}
    });
  }

  function submitEditUser(idUser){

    nameUser = document.getElementById("name").value;
    //email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    repassword = document.getElementById("repassword").value;
    group = document.getElementById("group").value;
    active = $('#active').is(":checked") ? 1 : 0;
    
    //refresh field error
    $(".form-group").find(".form-control").removeClass("is-invalid");
    $(".form-group").find(".invalid-feedback").html('');
    $.ajax({
      type: "post",
      url: APP_URL + '/submit-edit-user',
      headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data:{'id': idUser, 'name': nameUser, 'password': password, 'repassword': repassword, 'group': group, 'active': active} ,			  
      success: function(data){
          response = $.parseJSON(data);
          if(response['errorCode'] == 0){
              closePopup();
              var titleAlert = "Thành công !";
          }else{
              var titleAlert = "Có lỗi xảy ra !";
          }

          Swal.fire({
            icon: response['status'],
            title: titleAlert,
            text: response['message'],
          }).then(() => {
            window.location.reload();
          })

          
      },
      error: function(data){
        //Mã lỗi validation của Laravel
        if(data.status == '422'){
            //@param json
            response = JSON.parse(data['responseText']);
            $.each( response.errors, function( key, value ) {
                $("#group"+key).find(".form-control").addClass("is-invalid");
                $("#group"+key).find(".invalid-feedback").html(value);
            });
        }else{
            alert("Lỗi không xác định");return false;
        }
      }
    });

  }
  </script>
@endsection