<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Screen</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/adminTLE3/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="../../assets/adminTLE3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/adminTLE3/dist/css/adminlte.min.css">

    <!-- Login Screen style -->
    <link rel="stylesheet" href="../../assets/css/login.css">
    <!-- Action -->
    <script src="../../assets/js/action.js"></script>

    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

</head>
<body class="login-page">
    <div class="login-box">
    <div class="card">
        <div class="card-body login-card-body">
        <figure class="logo-river"><img src="../../assets/images/logo_rivercrane.png" /></figure><br />

        {{-- <!-- @if ($errors->any())
            <ul class="listError">
                @foreach ($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach
            </ul>
        @endif --> --}}

        @if(session('status'))
         <ul class="listError">
             <li class="text-danger"> {{ session('status') }}</li>
         </ul>
        @endif
        
        <div class="listError"><span class="text-danger"></span></div>
    
            <div class="input-group mb-3" id="groupemail">
                <input value="" id="emailInput" name="email" class="form-control" placeholder="Email" autocomplete="off">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-user"></span>
                    </div>
                </div>
                <span class="invalid-feedback"></span>
            </div>
            
            {{-- @error('email')
                <ul class="listError">
                    <li class="text-danger">{{ $message }}</li>
                </ul>
            @enderror --}}
            
            <div class="input-group mb-3" id="grouppassword">
                <input value="" id="passwordInput" name="password" type="password" class="form-control" placeholder="Mật khẩu" autocomplete="new-password">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                    </div>
                </div>
                <span class="invalid-feedback"></span>
            </div>
            
            {{-- @error('password')
                <ul class="listError">
                    <li class="text-danger">{{ $message }}</li>
                </ul>
            @enderror --}}
            
            <div class="row">
            <div class="col-6">
                <div class="icheck-primary">
                <input type="checkbox" id="remember" name="remember" autocomplete="off">
                <label for="remember">
                    Remember
                </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-6">
                <div onclick="submitLogin()" class="btn btn-primary btn-block">Đăng nhập</div>
            </div>
            <!-- <div class="col-6">
                <div onclick="dangNhapAjax();" class="btn btn-primary btn-block">Đăng nhập Ajax</div>
            </div> -->
            <!-- /.col -->
            </div>
        
        <!-- /.login-card-body -->

        
    </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="../../assets/adminTLE3/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../assets/adminTLE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../assets/adminTLE3/dist/js/adminlte.min.js"></script>

    <script>
        $(document).on('keypress',function(e) {
            if(e.which == 13) {
                submitLogin();
            }
        });
        function submitLogin(){
            
            email = document.getElementById("emailInput").value;
            password = document.getElementById("passwordInput").value;
            
            if($('#remember').is(':checked')){
                remember = true;
            }else{
                remember = false;
            }
            
            $(".input-group").find(".form-control").removeClass("is-invalid");
            $(".input-group").find(".invalid-feedback").html('');
            $(".listError .text-danger").html('');
            
            $.ajax({
            type: "post",
            url: APP_URL + '/login',
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},	
            data:{'email': email, 'password': password, 'remember': remember} ,			  
            success: function(data){
                //alert(data);return false;
                response = $.parseJSON(data);
                if(response['errorCode'] == 0){
                    window.location.reload();
                }else if(response['errorCode'] == 1){
                    $(".listError .text-danger").html(response['message']+'<br /><br />');
                }else{
                    alert("Xử lý user lỗi.");
                }
            },
            error: function(data){
                //Mã lỗi validation của Laravel
                if(data.status == '422'){
                    //@param json
                    response = JSON.parse(data['responseText']);
                    $.each( response.errors, function( key, value ) {
                        //alert(key);return false;
                        $("#group"+key).find(".form-control").addClass("is-invalid");
                        $("#group"+key).find(".invalid-feedback").html(value);
                    });
                }else{
                    //alert(data.status)
                    alert("Lỗi không xác định");return false;
                }
                
            }
        });
        }
    </script>


</body>
</html>
