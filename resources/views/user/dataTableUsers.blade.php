@if(count($arrData['users']) > 0)
<div class="row">
  <div class="col-md-12 col-lg-7">{{ $arrData['users']->onEachSide(2)->links() }}</div>
  <div style="text-align: right;" class="col-sm-12 col-lg-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Hiển thị từ {{ $arrData['users']->firstItem() }} ~ {{ $arrData['users']->lastItem() }} trong tổng số {{ $arrData['users']->total() }} user</div></div>
</div>
<div class="row">
    <table id="tableStyle" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Họ tên</th>
                <th>Email</th>
                <th>Nhóm</th>
                <th>Trạng thái</th>
                <th></th>
            </tr>
        </thead>

        <tbody id="bodyTable">
            <tr id="loadingGif"><td colspan="6"><div class="containerLoading"><img height="80" src="{{ URL::asset('assets/gifs/loading.gif') }}" /></div></td></tr>      
            @foreach($arrData['users'] as $index=>$user)
              <tr id="user_{{ $user->id }}">
                  <td>{{ $index+1 }}</td>
                  <td title="create At: {{ $user->created_at }}">{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->group_role }}</td>
                  <td class="status">
                    @if($user->is_active == 1)
                      <span class="text-success">Đang hoạt động</span>
                    @else
                      <span class="text-danger">Tạm khóa</span>
                    @endif
                  </td>
                  <td class="listFunctionInDataTable">
                    <i onclick="showPopupEditUser('{{ $user->id }}');" style="color:#0056b3" class="fas fa-pen"></i>
                    <i onclick="deleteUser('{{ $user->id }}','{{ $user->name }}')" style="color:red" class="fas fa-trash-alt"></i>
                    <i onclick="changeStatusUser('{{ $user->id }}','{{ $user->name }}', '{{ $user->is_active }}')" class="fas fa-user-times"></i>
                    
                    {{-- <span onclick="showPopupEditUser('{{ $user->id }}');" style="color:#0056b3">Sửa</span>
                    <span onclick="deleteUser('{{ $user->id }}','{{ $user->name }}')" style="color:red">Xóa</span>
                    <span onclick="changeStatusUser('{{ $user->id }}','{{ $user->name }}', '{{ $user->is_active }}')">Khóa</span> --}}
                    
                  </td>
              </tr>
            @endforeach
          
        </tbody>
    </table>
</div>
<div class="row col-12"><div class="list-page-link">{{ $arrData['users']->onEachSide(2)->links() }}</div></div>

@include('scriptUnbindPaginate')
@else
<tbody><tr><td>Không có dữ liệu hiển thị</td></tr></tbody>
@endif
