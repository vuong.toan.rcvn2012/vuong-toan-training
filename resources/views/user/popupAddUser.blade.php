<!-- Login Screen style -->
<link rel="stylesheet" href="../../assets/css/login.css">
<style>
    input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 1000px white inset !important;
    }
</style>
<div class="col-md-12">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">THÊM USER MỚI</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        
        <form id="quickForm" autocomplete="off">
      
        <div class="card-body">
            <div id="errorAlert" class="text-danger"></div><br />
            <div class="form-group" id="groupname">
                <label for="name">Họ & Tên (*)</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Nhập đầy đủ họ & tên" autocomplete="off">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="groupemail">
                <label for="email">Email (*)</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Nhập Email đúng định dạng" autocomplete="off">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="grouppassword">
                <label for="password">Mật khẩu (*)</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Nhập mật khẩu" autocomplete="new-password">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="grouprepassword">
                <label for="repassword">Xác nhận (*)</label>
                <input type="password" name="repassword" class="form-control" id="repassword" placeholder="Nhập lại mật khẩu" autocomplete="new-password">
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-group" id="groupgroup">
                <label for="exampleSelectRounded0">Nhóm (*)</label>
                <select class="group form-control custom-select rounded-0" id="group">
                    <option value ="" selected="" disabled>Chọn nhóm</option>
                    <option value="Admin">Admin</option>
                    <option value="Editor">Editor</option>
                    <option value="Reviewer">Reviewer</option>
                </select>
                <span class="error invalid-feedback"></span>
            </div>
            <div class="form-check" id="groupactive">
                <input type="checkbox" checked="" class="active form-check-input" id="active">
                <label class="form-check-label" for="active">Check trạng thái hoạt động</label>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div onclick="submitAddUser();" type="submit" class="col-3 btn btn-primary pull-right" style="float:right;margin-left:10px">Lưu</div> 
            <div onclick="closePopup();" class="col-3 btn btn-secondary" style="float:right">Hủy</div>
        </div>
        </form>
    </div>
</div>