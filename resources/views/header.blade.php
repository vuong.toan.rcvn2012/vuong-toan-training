<header>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="{{ URL::to('/') }}" class="navbar-brand"><img src="{{ URL::asset('assets/images/logo_rivercrane.png')}}" alt="AdminLTE Logo" class="brand-image" style="opacity: .8"></a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item {{ $arrData['currentView'] == 'listProduct' ? 'selected' : '' }}">
            <a href="{{ route('getListProduct') }}" class="nav-link">Sản phẩm</a>
            </li>
            <li class="nav-item {{ $arrData['currentView'] == 'listCustomer' ? 'selected' : '' }}">
            <a href="{{ route('getListCustomer') }}" class="nav-link">Khách hàng</a>
            </li>
            <li class="nav-item {{ $arrData['currentView'] == 'listUser' ? 'selected' : '' }}">
            <a href="{{ route('getListUser') }}" class="nav-link">Users</a>
            </li>
            <li class="nav-item {{ $arrData['currentView'] == 'listOrder' ? 'selected' : '' }}">
            <a href="{{ route('getListOrder') }}" class="nav-link">Đơn hàng</a>
            </li>
        </ul>

        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item dropdown">
            <a href="{{ route('getLogout') }}" class="nav-link">
            <i class="fas fa-user"></i>
            {{ Auth::user()->name }}, đăng xuất
            </a>
        </li>
        </ul>
    </div>
  </nav>
  <!-- /.navbar -->
</header>