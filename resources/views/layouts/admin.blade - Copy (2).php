<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/dist/css/adminlte.min.css') }}">
    <!-- table Screen style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/table.css') }} ">
    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    {{--Styles custom--}}
    @yield('styles')
</head>
<body class="hold-transition layout-top-nav">
  <div class="wrapper">
    @include('nav')
    <div class="content-wrapper">
      <div class="content-header">@yield('breadcrumbNav')</div>
      <div class="content">
        <div class="container">
          <section class="content">
            <div class="card">
              <div class="card-header">@yield('boxSearch')</div>
              <div id="cardBody" class="card-body">
                @yield('linkPageHeader')
                @yield('content')
                @yield('linkPageFooter')
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  @include('footer')
  {{--Scripts link to file or js custom--}}
  @yield('scriptProcess')
</body>
</html>