@include('header')
<body class="hold-transition layout-top-nav">
  <div class="wrapper">
    @include('nav')
    <div class="content-wrapper">
      <div class="content-header">@yield('breadcrumbNav')</div>
      <div class="content">
        <div class="container">
          <section class="content">
            <div class="card">
              <div class="card-header">@yield('boxSearch')</div>
              <div id="cardBody" class="card-body">
                @yield('linkPageHeader')
                @yield('dataTable')
                @yield('linkPageFooter')
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  @include('footer')
  @yield('scriptProcess')
</body>
</html>