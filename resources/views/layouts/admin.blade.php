<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/fontawesome-free/css/all.min.css') }}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css"> --}}
    
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/adminTLE3/dist/css/adminlte.min.css') }}">
    <!-- table Screen style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/table.css') }} ">
    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    {{--Styles custom--}}
    @yield('styles')
</head>
<body class="hold-transition layout-top-nav">
  <div class="wrapper">
    @include('header')
    <div class="content-wrapper" style="padding:5px 0 30px">
      <div class="content-header">
        <div class="container container-main">
          <div class="row mb-2">
            @section('breadcrumbNav')
            <div class="col-sm-6">
              <h1 class="m-0">@yield('title')</h1>
            </div>
            @show
          </div>
        </div>
      </div>
      <div class="content">
        <div class="container container-main">
          <section class="content">
            <div class="card card-main" style="margin-bottom:0;">
              @yield('content')
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  
  @include('footer')

  <!-- REQUIRED COMMON SCRIPTS -->
  <!-- jQuery -->
  <script src="{{ URL::asset('assets/adminTLE3/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ URL::asset('assets/adminTLE3/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  
  <!-- Popup -->
  <script src="{{ URL::asset('assets/js/popup.js') }}"></script>
  <!-- Action -->
  <script src="{{ URL::asset('assets/js/action.js') }}"></script>
  
  <!-- AdminLTE App -->
  <script src="{{ URL::asset('assets/adminTLE3/dist/js/adminlte.min.js') }}"></script>
  <!-- Page specific script -->
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  {{--Scripts link to file or js custom--}}
  @yield('scriptProcess')
</body>
</html>