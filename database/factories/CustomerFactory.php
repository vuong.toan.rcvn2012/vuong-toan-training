<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CustomerModel;
use Faker\Generator as Faker;

$factory->define(CustomerModel::class, function (Faker $faker) {
    $arrRandomGroup = array("090","097","098","077","092","070");
    $random_keys=array_rand($arrRandomGroup,1);
    return [
        'customer_name' => $faker->name,
        'email'         => $faker->safeEmail,
        'tel_num'       => $arrRandomGroup[$random_keys].$faker->numerify('#######'),
        'address'       => $faker->address,
    ];
});
