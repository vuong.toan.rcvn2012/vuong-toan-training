<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductModel;
use Faker\Generator as Faker;
$factory->define(ProductModel::class, function (Faker $faker) {
    return [
        'product_id'        => "S".Str::random(10),
        'product_name'         => "Sản phẩm A",
        'description'       => "Mô tả sản phẩm A",
    ];
});
