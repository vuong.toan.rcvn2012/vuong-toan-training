<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        // DB::table('users')->insert([
        //     ['name' => 'Vương Toàn River Crane', 'email' => 'vuong.toan.rcvn2012@gmail.com', 'password' => '$2y$10$I8KSaFi/kebIMtJVK6pBy.L4reQjYvNjj8IwcTMrz4HN0yX2v4H0a'],
        //     ['name' => 'Vương Toàn Blockphp', 'email' => 'vuong.toan.rcvn2021@gmail.com', 'password' => '$2y$10$I8KSaFi/kebIMtJVK6pBy.L4reQjYvNjj8IwcTMrz4HN0yX2v4H0a']
        // ]);
    }
}
